﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;

namespace UpdateTheme
{
    class Program
    {
        static log4net.ILog logger = null;
        static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                logger = log4net.LogManager.GetLogger("File");
                logger.Info("Starting TCM Updater");

                UpdateTCMLinks();

                logger.Info("Done--------------------------------------");
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        static void LogError(Exception ex)
        {
            logger.Error(ex.Message);
            logger.Error(ex.StackTrace);
        }

        static void UpdateTCMLinks()
        {
            int count = 0;
            ArrayList URLsToAvoid = new ArrayList();
            //V4 server
            URLsToAvoid.Add("http://v4dev");
            URLsToAvoid.Add("http://v4dev:36306");
            URLsToAvoid.Add("http://v4dev:36306/sites/Help");
            URLsToAvoid.Add("https://sas.sharevision.ca");
            URLsToAvoid.Add("http://vserver14760:38673");
            URLsToAvoid.Add("http://vserver14760:38673/sites/Help");
            URLsToAvoid.Add("https://testsite2013.sharevision.ca");
            SPFarm farm = SPFarm.Local;
            foreach (SPService objService in farm.Services)
            {
                if (objService is SPWebService)
                {
                    SPWebService oWebService = (SPWebService)objService;
                    foreach (SPWebApplication webApp in oWebService.WebApplications)
                    {
                        SPSiteCollection siteCollections = webApp.Sites;
                        foreach (SPSite siteCollection in siteCollections)
                        {
                            if (URLsToAvoid.Contains(siteCollection.Url))
                                continue;
                            SPWeb oWeb = siteCollection.OpenWeb();
                            SPList list = null;
                            try
                            {
                                string path = "";
                                string SiteUrl = oWeb.Url;
                                int thirdSlash = SiteUrl.IndexOf("/", 8);
                                if (thirdSlash > 0)  //Non-Subsites will such as Inclusionpowellriver will not have a 3rd slash, and this variable will be -1.
                                    path = SiteUrl.Substring(thirdSlash);

                                list = oWeb.GetList(path + "/lists/Site%20Theme/"); //If the list does not exist at this url, an exception is thrown.
                            }
                            catch (Exception ee)
                            {
                                list = null;
                            }

                            logger.Info("Processing " + oWeb.Url);

                            if (list != null)
                            {
                                if (list.Fields.ContainsField("GridEventHighlight") == false)
                                {
                                    SPFieldText fld1 = (SPFieldText)list.Fields.CreateNewField(SPFieldType.Text.ToString(), "GridEventHighlight");
                                    //fld1.Title = "Grid Event Highlight";
                                    list.Fields.Add(fld1);

                                    SPFieldText fld2 = (SPFieldText)list.Fields.CreateNewField(SPFieldType.Text.ToString(), "GridEventColor");
                                    //fld2.Title = "Grid Event Color";
                                    list.Fields.Add(fld2);

                                    list.Update();
                                }


                                    SPListItemCollection updateItems = list.Items;
                                foreach (SPListItem item in updateItems)
                                {
                                    string themeName = "";
                                    if (list.Fields.ContainsField("Theme_x0020_Name"))
                                    {
                                        themeName = item["Theme_x0020_Name"] + "";
                                    }
                                    else
                                    {
                                        themeName = item["Name"] + "";
                                    }

                                    

                                    string headerColor = item["Header_x0020_Colour"] + "";
                                    //3b0093
                                    if (themeName.Contains("4") || (themeName.Contains("Active") && headerColor.Contains("3b0093")))
                                    {
                                        item["GridEventHighlight"] = "rgba(177, 125, 255, 1)";
                                        item["GridEventColor"] = "#ffffff";
                                    }
                                    else if (themeName.Contains("ShareVision") || (themeName.Contains("Active") && headerColor.Contains("624383")))
                                    {
                                        item["GridEventHighlight"] = "rgba(255, 217, 125, 0.98)";
                                        item["GridEventColor"] = "#000000";
                                    }
                                    else if (themeName.Contains("Blue and Gold") || (themeName.Contains("Active") && headerColor.Contains("2349a1")))
                                    {
                                        item["GridEventHighlight"] = "rgba(63, 146, 212, 1)";
                                        item["GridEventColor"] = "#ffffff";
                                    }
                                    else if (themeName.Contains("Multicolour") || (themeName.Contains("Active") && headerColor.Contains("892a90")))
                                    {
                                        item["GridEventHighlight"] = "rgba(249, 154, 41, 0.51)";
                                        item["GridEventColor"] = "#000000";
                                    }
                                    else if (themeName.Contains("Community Living") || (themeName.Contains("Active") && headerColor.Contains("003797")))
                                    {
                                        item["GridEventHighlight"] = "rgba(255, 180, 0, 0.64)";
                                        item["GridEventColor"] = "#000000";
                                    }

                                    item.Update();



                                }


                            }
                            else
                            {
                                logger.Info("Site Theme list is not found");
                            }

                        }
                    }
                }
            }

            logger.Info("updated " + count + " sites");
        }

        static void UpdateTCM()
        {
            ArrayList URLsToAvoid = new ArrayList();
            //V4 server
            URLsToAvoid.Add("http://v4dev");
            URLsToAvoid.Add("http://v4dev:36306");
            URLsToAvoid.Add("http://v4dev:36306/sites/Help");
            URLsToAvoid.Add("https://sas.sharevision.ca");
            URLsToAvoid.Add("http://vserver14760:38673");
            URLsToAvoid.Add("http://vserver14760:38673/sites/Help");
            URLsToAvoid.Add("https://testsite2013.sharevision.ca");
            SPFarm farm = SPFarm.Local;
            foreach (SPService objService in farm.Services)
            {
                if (objService is SPWebService)
                {
                    SPWebService oWebService = (SPWebService)objService;
                    foreach (SPWebApplication webApp in oWebService.WebApplications)
                    {
                        SPSiteCollection siteCollections = webApp.Sites;
                        foreach (SPSite siteCollection in siteCollections)
                        {
                            if (URLsToAvoid.Contains(siteCollection.Url))
                                continue;
                            SPWeb oWeb = siteCollection.OpenWeb();
                            SPList list = null;
                            try
                            {
                                string path = "";
                                string SiteUrl = oWeb.Url;
                                int thirdSlash = SiteUrl.IndexOf("/", 8);
                                if (thirdSlash > 0)  //Non-Subsites will such as Inclusionpowellriver will not have a 3rd slash, and this variable will be -1.
                                    path = SiteUrl.Substring(thirdSlash);

                                list = oWeb.GetList(path + "/lists/TrainingCourseConfig"); //If the list does not exist at this url, an exception is thrown.
                            }
                            catch (Exception ee)
                            {
                                list = null;
                            }

                            logger.Info("Processing " + oWeb.Url);

                            if (list != null)
                            {
                                //PassPercentage
                                logger.Info("TCM list has " + list.ItemCount + " items");
                                //figure out how many has null value in PassPercentage
                                SPQuery query = new SPQuery();
                                query.Query = "<Where><IsNull><FieldRef Name='PassPercentage' /></IsNull></Where>";
                                SPListItemCollection items = list.GetItems(query);
                                logger.Info("TCM list has " + items.Count + " items with no PassPercentage value");

                                SPField fld = list.Fields.GetField("PassPercentage");
                                //update TCM items
                                foreach (SPListItem item in items)
                                {
                                    //string courseName = item["CourseName"] + "";
                                    //logger.Info(courseName);
                                    item[fld.Id] = 0;
                                    item.Update();
                                }

                                //update field
                                fld.Required = true;
                                fld.Update();


                            }
                            else
                            {
                                logger.Info("TCM list is not found");
                            }

                        }
                    }
                }
            }
        }
    }
}
