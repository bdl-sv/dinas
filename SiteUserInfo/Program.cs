﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;


namespace SiteUserInfo
{
    class Program
    {
        static log4net.ILog logger = null;
        static List<string> URLsToAvoid = new List<string>();
        static string STAFF_LIST_ID = "{08c3de5a-f007-4eb1-8081-ef91c908cd0c}";
        static List<string> BDLUsers = new List<string>();

        static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                logger = log4net.LogManager.GetLogger("File");
                logger.Info("Starting GT-282");

                BDLUsers.Add("dina");
                BDLUsers.Add("alex.s");
                BDLUsers.Add("asraful");
                BDLUsers.Add("ben");
                BDLUsers.Add("breakwater");
                BDLUsers.Add("brittany.h");
                BDLUsers.Add("dion");
                BDLUsers.Add("donlr");
                BDLUsers.Add("jamie");
                BDLUsers.Add("justin");
                BDLUsers.Add("kathleen");
                BDLUsers.Add("kristen.finnsson");
                BDLUsers.Add("donlr");
                BDLUsers.Add("laurie");
                BDLUsers.Add("lucas");
                BDLUsers.Add("maksud");
                BDLUsers.Add("mike");
                BDLUsers.Add("philklassen");
                BDLUsers.Add("rajib");
                BDLUsers.Add("ronb");
                BDLUsers.Add("wahl");
                BDLUsers.Add("dbaccess");
                BDLUsers.Add("sv.expert");
                BDLUsers.Add("sv.admin");
                BDLUsers.Add("marty");
                BDLUsers.Add("nazmul");
                BDLUsers.Add("steve");
                BDLUsers.Add("sven");
                BDLUsers.Add("macy");


                ListSites();

                logger.Info("Done--------------------------------------");
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }


        static void ListSites()
        {
            SPFarm farm = SPFarm.Local;
            foreach (SPService objService in farm.Services)
            {
                if (objService is SPWebService)
                {
                    SPWebService oWebService = (SPWebService)objService;
                    foreach (SPWebApplication webApp in oWebService.WebApplications)
                    {
                        SPSiteCollection siteCollections = webApp.Sites;
                        foreach (SPSite siteCollection in siteCollections)
                        {
                            if (URLsToAvoid.Contains(siteCollection.Url))
                                continue;

                            SiteStats(siteCollection);

                        }
                    }
                }
            }
        }

        static void SiteStats(SPSite site)
        {
            try
            {
                SPWeb web = site.OpenWeb();

                logger.Info("====================================================================");
                logger.Info(web.Title);
                logger.Info(web.Url);

                //logger.Info("Site Users " + web.SiteUsers.Count);
                //logger.Info("SCA Users " + web.SiteAdministrators.Count);

                int siteUser = 0;
                int scaUsers = 0;


                foreach (SPUser user in web.SiteUsers)
                {
                    if (user.LoginName.Contains("test") == false)
                    {
                        siteUser++;
                    }
                }


                foreach (SPUser user in web.SiteAdministrators)
                {

                    string login = user.LoginName.Split('\\')[1];


                    if (BDLUsers.Contains(login) == false)
                    {
                        //scaUsers++;
                        //logger.Info("SCA user " + user.LoginName);
                    }
                }

                foreach (SPGroup grp in web.SiteGroups)
                {
                    if (grp.Name == "GRP Site Administrators")
                    {
                        foreach (SPUser user in grp.Users)
                        {
                            string login = user.LoginName.Split('\\')[1];


                            if (BDLUsers.Contains(login) == false)
                            {
                                scaUsers++;
                            }
                        }


                        break;
                    }
                }

                logger.Info("Site Users " + siteUser);
                logger.Info("GRP Site Administrators " + scaUsers);


                SPList list = web.SiteUserInfoList;//web.Lists[new Guid(STAFF_LIST_ID)];
                if (list.Fields.ContainsField("Category"))
                {
                    SPFieldChoice fld = (SPFieldChoice)list.Fields.GetField("Category");

                    foreach (string choice in fld.Choices)
                    {
                        SPQuery query = new SPQuery();
                        query.Query = "<Where><Eq><FieldRef Name='Category' /><Value Type='Text'>" + choice + "</Value></Eq></Where>";

                        SPListItemCollection items = list.GetItems(query);

                        if (items.Count > 0)
                        {
                            logger.Info("Category " + choice + "(" + items.Count + ")");
                        }


                    }

                    SPQuery query1 = new SPQuery();
                    query1.Query = "<Where><IsNull><FieldRef Name='Category' /></IsNull></Where>";
                    SPListItemCollection items1 = list.GetItems(query1);
                    if (items1.Count > 0)
                    {
                        logger.Info("User without category " + items1.Count);
                    }


                }
                else
                {
                    logger.Info("Category field not found");
                }

            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        static void LogError(Exception ex)
        {
            logger.Error(ex.Message);
            //logger.Error(ex.StackTrace);
        }
    }
}
