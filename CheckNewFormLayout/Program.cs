﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;

namespace CheckNewFormLayout
{
    class Program
    {
        static log4net.ILog logger = null;
        static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                logger = log4net.LogManager.GetLogger("File");
                logger.Info("Starting Check Form Layout");

                CheckNewFormLayout();

                logger.Info("Done--------------------------------------");
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }


        static void CheckNewFormLayout()
        {
            string conn = ConfigurationManager.ConnectionStrings["TenantContext"].ConnectionString;
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
            {
                connection.Open();
                System.Data.SqlClient.SqlCommand command;
                System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                String sql = "SELECT * FROM [dbo].[Tenant] ORDER BY v4Host";
                command = new System.Data.SqlClient.SqlCommand(sql, connection);
                adapter.SelectCommand = command;
                System.Data.DataSet ds = new System.Data.DataSet();
                adapter.Fill(ds, "Tenant");

                foreach (System.Data.DataTable table in ds.Tables)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        string connx = row["Connection"] + "";
                        string v4Host = row["v4Host"] + "";
                        string spHost = row["SpHost"] + "";

                        logger.Info("v4Host " + v4Host + " - spHost " + spHost);
                        CheckDB(connx);
                    }
                }
            }
        }


        static void CheckDB(string connectionString)
        {
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(connectionString))
            {
                connection.Open();
                System.Data.SqlClient.SqlCommand selectCommand = new System.Data.SqlClient.SqlCommand("SELECT * FROM [dbo].[Menu]", connection);
                System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                adapter.SelectCommand = selectCommand;
                System.Data.DataTable ds = new System.Data.DataTable();
                adapter.Fill(ds);
            }
        }

        static void LogError(Exception ex)
        {
            logger.Error(ex.Message);
            logger.Error(ex.StackTrace);
        }
    }
}
