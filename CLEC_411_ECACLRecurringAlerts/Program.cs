﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System.Net.Mail;
using System.Collections.Specialized;

namespace CLEC_411_ECACLRecurringAlerts
{
    class Program
    {

        const string CONFIG_RECURRING_ALERTS_LIST_ID = "{92F3A9FB-1027-41B3-8EF1-2687F9A3B858}";
        const string CONFIG_RECURRING_ALERTS_LOG_LIST_ID = "{2e02e221-406d-4f03-a127-f1ed14b17323}";

        static SPSite oSite = null;
        static SPWeb oWeb = null;
        static log4net.ILog logger = null;

        static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                logger = log4net.LogManager.GetLogger("File");
                logger.Info("Starting Recurring Alerts");

                oSite = new SPSite("https://sp2013.communitylivingessex.org");
                oWeb = oSite.RootWeb;

                SendAlerts(oWeb);
                logger.Info("End");

            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            finally
            {
                try
                {
                    if (oWeb != null)
                        oWeb.Dispose();
                }
                catch { }

                try
                {
                    if (oSite != null)
                        oSite.Dispose();
                }
                catch { }
            }
        }

        static void SendAlerts(SPWeb oWeb)
        {
            int emailsSent = 0;

            try
            {
                SPQuery query = new SPQuery();
                query.Query = "<Where><And><And><Eq><FieldRef Name='Active' /><Value Type='Integer'>1</Value></Eq><Leq><FieldRef Name='StartDate' /><Value Type='DateTime'><Today /></Value></Leq></And><Or><IsNull><FieldRef Name='EndDate' /></IsNull><Gt><FieldRef Name='EndDate' /><Value Type='DateTime'><Today /></Value></Gt></Or></And></Where>";
                SPList logList = oWeb.Lists[new Guid(CONFIG_RECURRING_ALERTS_LOG_LIST_ID)];

                SPListItemCollection items = oWeb.Lists[new Guid(CONFIG_RECURRING_ALERTS_LIST_ID)].GetItems(query);

                logger.Info("Items found to possibly send out: " + items.Count);                

                foreach (SPListItem item in items)
                {
                    string patttern = (string)item[item.Fields.GetField("RecurrencePattern").Id];
                    DateTime startDate = (DateTime)item[item.Fields.GetField("Created").Id];
                    if (item[item.Fields.GetField("StartDate").Id] != null)
                        startDate = (DateTime)item[item.Fields.GetField("StartDate").Id];

                    bool send = false;
                    if (startDate.Date == DateTime.Today)
                        send = true;

                    if (patttern == "Every x months")
                    {
                        double? months = (double?)item[item.Fields.GetField("Months").Id];
                        if (months != null)
                        {
                            DateTime sendDate = startDate;
                            while (sendDate < DateTime.Today)
                            {
                                sendDate = sendDate.AddMonths((int)months.Value);
                            }
                            if (sendDate.Date == DateTime.Today)
                                send = true;
                        }
                    }
                    else if (patttern == "Every x days")
                    {
                        double? days = (double?)item[item.Fields.GetField("Days").Id];
                        if (days != null)
                        {
                            DateTime sendDate = startDate;
                            while (sendDate < DateTime.Today)
                            {
                                sendDate = sendDate.AddDays(days.Value);
                            }
                            if (sendDate.Date == DateTime.Today)
                                send = true;
                        }
                    }

                    if (send)
                    {
                        string body = (string)item[item.Fields.GetField("EmailBody").Id];
                        if (body != null)
                        {
                            body = body.Replace("href=\"/", "href=\"" + oWeb.Url + "/");
                        }
                        string subject = (string)item[item.Fields.GetField("Subject").Id];
                        string addAlert = (string)item[item.Fields.GetField("AdditionallyAlert").Id];
                        string to = "";
                        List<SPUser> users = GetUsers(item, item.Fields.GetField("Sendto"), oWeb);
                        foreach (SPUser user in users)
                        {
                            if (user.Email != null)
                                to = to + user.Email + ";";
                        }
                        to += addAlert;
                        if (!String.IsNullOrEmpty(to))
                        {
                            SendEmail(to, subject, body);
                            emailsSent++;

                            //create a log
                            SPListItem log = logList.Items.Add();
                            log["ConfigRecurringAlertsID"] = item.ID;
                            log["To"] = to.Replace(";", "; ");
                            log["Subject"] = subject;
                            log["Body"] = body;
                            log.Update();
                        }
                        else
                        {
                            logger.Info("CONFIG ITEM ID  " + item.ID + " - did not find email recipients");
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            logger.Info("Emails Sent: " + emailsSent);
        }

        static List<SPUser> GetUsers(SPListItem item, SPField field, SPWeb web)
        {
            List<SPUser> users = new List<SPUser>();

            try
            {
                SPFieldUserValueCollection groups = new SPFieldUserValueCollection(web, item[field.Id].ToString());
                if (groups != null)
                {

                    foreach (SPFieldUserValue userVal in groups)
                    {
                        if (userVal.User != null)
                        {
                            users.Add(userVal.User);
                        }
                        else
                        {
                            SPGroup group = web.SiteGroups.GetByID(userVal.LookupId);
                            foreach (SPUser user in group.Users)
                            {
                                users.Add(user);
                            }
                        }
                    }
                }
            }
            catch { }

            return users;
        }

        static void LogError(Exception ex)
        {
            logger.Error(ex.Message);
            logger.Error(ex.ToString());
        }
                        
        private static void SendEmail(string to, string subject, string emailBody)
        {
            int loc = 0;
            try
            {
                List<string> usedEmails = new List<string>();
                string fromEmail = "admin@sharevision.ca";
                string replyToEmail = "admin@sharevision.ca";

                MailAddress oFrom = new MailAddress(fromEmail, "ShareVision Alerts");
                MailAddress oReplyTo = new MailAddress(replyToEmail, "ShareVision");
                SmtpClient oClient = new SmtpClient("smtp.sendgrid.net", 587);
                MailMessage oMsg = new MailMessage();
                oMsg.IsBodyHtml = true;

                string[] emails = to.Split(';');
                foreach (string em in emails)
                {
                    string addr = em.Trim();
                    if (addr == "" || !IsValidEmail(addr))
                        continue;
                    oMsg.To.Add(new MailAddress(addr));
                }

                oMsg.From = oFrom;
                oMsg.Subject = subject;
                oMsg.ReplyToList.Add(replyToEmail);
                oMsg.Body = emailBody;

                oClient.UseDefaultCredentials = false;
                oClient.EnableSsl = true;
                string emailUser = System.Configuration.ConfigurationManager.AppSettings["email_user"];
                string emailPass = System.Configuration.ConfigurationManager.AppSettings["email_password"];
                oClient.Credentials = new System.Net.NetworkCredential(emailUser, emailPass);                
                oClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                oClient.Send(oMsg);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw ex;
            }           
        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                MailAddress addr = new MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
