﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;

namespace CLEC_400_PersonalSpending
{
    class Program
    {
        static log4net.ILog logger = null;
        static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                logger = log4net.LogManager.GetLogger("File");
                logger.Info("Starting Spending Money Request Customization. CLEC-400");
                UncheckOnHoldForPastItems();
                logger.Info("Done--------------------------------------");
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        static void LogError(Exception ex)
        {
            logger.Error(ex.Message);
            logger.Error(ex.StackTrace);
        }

        static void UncheckOnHoldForPastItems()
        {
            SPSite oSite = null;
            SPWeb oWeb = null;

            try
            {                
                oSite = new SPSite("https://sp2013.communitylivingessex.org/");
                oWeb = oSite.OpenWeb();
                oWeb.AllowUnsafeUpdates = true;

                SPList oSpendingList = oWeb.Lists[new Guid("{ec304eeb-1efa-4a5f-b0e5-d6eca4b3e664}")];
                SPQuery oQuery = new SPQuery();
                oQuery.Query = "<Where><And><Leq><FieldRef Name='PaymentonHoldEndDate' /><Value Type='DateTime'><Today/></Value></Leq><Eq><FieldRef Name='PaymentonHold' /><Value Type='Boolean'>1</Value></Eq></And></Where>";
                SPListItemCollection oItems = oSpendingList.GetItems(oQuery);

                Guid gOnHold = oSpendingList.Fields.GetFieldByInternalName("PaymentonHold").Id;

                for (int i = oItems.Count - 1; i >= 0; i --)
                {
                    SPListItem item = oItems[i];
                    item[gOnHold] = false;
                    item.Update();
                }

                logger.Info("Updated " + oItems.Count + " items");
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            finally
            {
                if (oWeb != null)
                {
                    oWeb.AllowUnsafeUpdates = false;
                    oWeb.Dispose();
                }
                if (oSite != null) oSite.Dispose();
            }
        }


    }
}
