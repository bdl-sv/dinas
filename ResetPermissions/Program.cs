﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.SharePoint;


namespace ResetPermissions
{
    class Program
    {
        static log4net.ILog logger = null;
        static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                logger = log4net.LogManager.GetLogger("File");
                logger.Info("Starting Reset List / Item Permissions");

                ResetPermissions();


                logger.Info("Done--------------------------------------");
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }
        static void ResetPermissions()
        {
            Guid profileList = new Guid("{8ABCFCD6-8D66-4D54-98E1-91754B27F2AE}");
            Guid programsList = new Guid("{459CFC7D-4D56-4529-AE1F-55ED7744FE83}");

            string spSiteUrl = ConfigurationManager.AppSettings["SPSiteUrl"];
            using (SPSite site = new SPSite(spSiteUrl))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    SPListCollection lists = web.Lists;
                    foreach (SPList list in lists)
                    {
                      

                        if (list.Title == "Colour Calendar Mapping" || list.Title == "Converted Forms" || list.Title == "DataConnLib")
                            continue;

                        if (list.ID.Equals(profileList) || list.ID.Equals(programsList))
                            continue;

                        if (list.Hidden)
                            continue;

                        if (list.HasUniqueRoleAssignments)
                        {
                            logger.Info(list.Title + " - HasUniqueRoleAssignments =" + list.HasUniqueRoleAssignments + " - Hidden=" + list.Hidden);
                            list.ResetRoleInheritance();
                            
                        }


                        SPListItemCollection items = list.Items;
                        foreach (SPListItem item in items)
                        {
                            if (item.HasUniqueRoleAssignments)
                            {
                                item.ResetRoleInheritance();
                            }
                        }

                    }
                }
            }
        }
        static void LogError(Exception ex)
        {
            logger.Error(ex.Message);
            logger.Error(ex.StackTrace);
        }
    }
}
