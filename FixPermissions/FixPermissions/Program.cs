﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System.Net;
using System.Xml;
using System.Collections;
using System.Data.SqlClient;

namespace FixPermissions
{
    class Program
    {
        static log4net.ILog logger = null;

        static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                logger = log4net.LogManager.GetLogger("File");
                logger.Info("Starting Fix List / Item Permissions");

                RemoveAlertMe();


                logger.Info("Done--------------------------------------");
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }


        static void RemoveAlertMe()
        {
            ArrayList URLsToAvoid = new ArrayList();
            //V4 server
            URLsToAvoid.Add("http://v4dev");
            URLsToAvoid.Add("http://v4dev:36306");
            URLsToAvoid.Add("http://v4dev:36306/sites/Help");
            URLsToAvoid.Add("https://sas.sharevision.ca");
            URLsToAvoid.Add("http://vserver14760:38673");
            URLsToAvoid.Add("http://vserver14760:38673/sites/Help");
            URLsToAvoid.Add("https://testsite2013.sharevision.ca");
            SPFarm farm = SPFarm.Local;
            foreach (SPService objService in farm.Services)
            {
                if (objService is SPWebService)
                {
                    SPWebService oWebService = (SPWebService)objService;
                    foreach (SPWebApplication webApp in oWebService.WebApplications)
                    {
                        SPSiteCollection siteCollections = webApp.Sites;
                        foreach (SPSite siteCollection in siteCollections)
                        {
                            if (URLsToAvoid.Contains(siteCollection.Url))
                                continue;

                            webApp.AlertsEnabled = false;
                            webApp.Update();
                        }
                    }
                }
            }
        }
        static void UpdatePermissions()
        {
            string spSiteUrl = ConfigurationManager.AppSettings["SPSiteUrl"];
            int adminGroupId = Convert.ToInt32(ConfigurationManager.AppSettings["AdminGroupId"]);
           

            using (SPSite site = new SPSite(spSiteUrl))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    SPGroup adminGroup = web.SiteGroups.GetByID(adminGroupId);
                    SPListCollection lists = web.Lists;
                    SPRoleDefinition roleDef = web.RoleDefinitions.GetByType(SPRoleType.Administrator);

                    foreach (SPList list in lists)
                    {
                        //logger.Info("Processing list - " + list.Title);
                        if (list.Title == "Colour Calendar Mapping" || list.Title == "Converted Forms" || list.Title == "DataConnLib")
                            continue;

                        if (list.Hidden == false && list.HasUniqueRoleAssignments && HasGroup(list.RoleAssignments, roleDef, adminGroupId) == false)
                        {
                            logger.Info("Adding admin group to list " + list.Title);
                            SPRoleAssignment ra = new SPRoleAssignment(adminGroup);
                            ra.RoleDefinitionBindings.Add(roleDef);
                            list.RoleAssignments.Add(ra);
                        }

                        
                        SPListItemCollection items = list.Items;
                        //logger.Info("Processing list ITEMS - " + list.Title + " - count(" + items.Count + ")");
                        foreach (SPListItem item in items)
                        {
                            if (item.HasUniqueRoleAssignments && HasGroup(item.RoleAssignments, roleDef, adminGroupId) == false)
                            {
                                logger.Info("Adding admin group to ITEM " + item.ID + " - list = " + list.Title);
                                SPRoleAssignment ra = new SPRoleAssignment(adminGroup);
                                ra.RoleDefinitionBindings.Add(roleDef);
                                item.RoleAssignments.Add(ra);
                            }
                        }
                    }
                }
            }
        }

        static bool HasGroup(SPRoleAssignmentCollection roles, SPRoleDefinition roleDef, int groupId)
        {
            bool hasGroup = false;

            foreach (SPRoleAssignment ra in roles)
            {
                if (ra.Member.ID == groupId && ra.RoleDefinitionBindings.Contains(roleDef))
                {
                    hasGroup = true;
                }
            }

            return hasGroup;
        }

        static void LogError(Exception ex)
        {
            logger.Error(ex.Message);
            logger.Error(ex.StackTrace);
        }
    }
}
