﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System.Net;
using System.Xml;
using System.Collections;
using System.Data.SqlClient;


namespace FeaturesV4Plus
{
    class Program
    {
        static log4net.ILog logger = null;
        static List<string> V4Orgs = new List<string>();

        static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                logger = log4net.LogManager.GetLogger("File");
                logger.Info("Starting V4 Features");

                GetV4PlusSites();
                UpdateSites();

                logger.Info("Done--------------------------------------");
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        static void UpdateSites()
        {
            ArrayList URLsToAvoid = new ArrayList();
            //V4 server
            URLsToAvoid.Add("http://v4dev");
            URLsToAvoid.Add("http://v4dev:36306");
            URLsToAvoid.Add("http://v4dev:36306/sites/Help");
            URLsToAvoid.Add("https://sas.sharevision.ca");
            URLsToAvoid.Add("http://vserver14760:38673");
            URLsToAvoid.Add("http://vserver14760:38673/sites/Help");
            URLsToAvoid.Add("https://testsite2013.sharevision.ca");
            SPFarm farm = SPFarm.Local;
            foreach (SPService objService in farm.Services)
            {
                if (objService is SPWebService)
                {
                    SPWebService oWebService = (SPWebService)objService;
                    foreach (SPWebApplication webApp in oWebService.WebApplications)
                    {
                        SPSiteCollection siteCollections = webApp.Sites;
                        foreach (SPSite siteCollection in siteCollections)
                        {
                            if (URLsToAvoid.Contains(siteCollection.Url))
                                continue;

                            if (V4Orgs.Contains(siteCollection.Url))
                            {
                                SPWeb oWeb = siteCollection.OpenWeb();

                                try
                                {
                                    SPList copyList = oWeb.GetList(oWeb.Url + "/Lists/ConfigNewFromExisting");
                                    if (copyList.HasUniqueRoleAssignments)
                                        copyList.ResetRoleInheritance();
                                    copyList.Hidden = false;
                                    copyList.Update();

                                    //update feature
                                    UpdateFeature(siteCollection.Url);
                                    logger.Info("Updated " + siteCollection.Url);
                                } 
                                catch (Exception ex)
                                {
                                    LogError(ex);
                                }

                            }
                        }
                    }
                }
            }
        }

        static void UpdateFeature(string siteUrl)
        {
            string connString = GetConnectionString(siteUrl);
            using (SqlConnection connection = new SqlConnection(connString))
            {
                connection.Open();
                System.Data.SqlClient.SqlCommand selectCommand = new System.Data.SqlClient.SqlCommand("SELECT * FROM [dbo].[Features] WHERE FeatureType = 2026", connection);
                System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                adapter.SelectCommand = selectCommand;
                System.Data.DataTable ds = new System.Data.DataTable();
                adapter.Fill(ds);
                foreach (System.Data.DataRow row in ds.Rows)
                {
                    row["Enabled"] = 1;
                }

                System.Data.SqlClient.SqlCommand updateCommand = new System.Data.SqlClient.SqlCommand("UPDATE [dbo].[Features] SET Enabled = 1 WHERE FeatureID = @FeatureID", connection);
                updateCommand.Parameters.Add("@Enabled", System.Data.SqlDbType.Bit, 1, "Enabled");
                updateCommand.Parameters.Add("@FeatureID", System.Data.SqlDbType.Int, 4, "FeatureID");
                adapter.UpdateCommand = updateCommand;
                adapter.Update(ds);
                selectCommand.Dispose();
                adapter.Dispose();
            }
        }

        static void GetV4PlusSites()
        {
            try
            {
                string query = "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                        "<soap:Body><GetListItems xmlns=\"http://schemas.microsoft.com/sharepoint/soap/\"><listName>Directory</listName>" +
                        "<query><Query><Where><And><Eq><FieldRef Name='_x0056_4' /><Value Type='Integer'>1</Value></Eq>" +
                        "<Eq><FieldRef Name='SV_x0020_Version' /><Value Type='Text'>V4</Value></Eq>" + 
                        "</And></Where></Query></query>" +
                        "</GetListItems></soap:Body></soap:Envelope>";

                using (WebClient client = new WebClient())
                {
                    client.Headers.Add(HttpRequestHeader.ContentType, "text/xml");
                    client.Headers.Add("SOAPAction", "http://schemas.microsoft.com/sharepoint/soap/GetListItems");
                    client.Credentials = new NetworkCredential("dbaccess", "4getful1", "SERVER8525");

                    string resOrgs = client.UploadString("https://trax.sharevision.ca/_vti_bin/lists.asmx", query);

                    XmlDocument docOrgs = new XmlDocument();
                    docOrgs.LoadXml(resOrgs);

                    XmlNodeList orgs = docOrgs.GetElementsByTagName("z:row");

                    foreach (XmlNode node in orgs)
                    {
                        if (node.Attributes["ows_V4_x0020_SP_x0020_Site_x0020_Url"] != null)
                        {
                            string orgUrl = node.Attributes["ows_V4_x0020_SP_x0020_Site_x0020_Url"].Value;
                            orgUrl = orgUrl.Split(',')[0];
                            if (orgUrl.EndsWith("/"))
                            {
                                orgUrl = orgUrl.Substring(0, orgUrl.Length - 1);
                            }
                            V4Orgs.Add(orgUrl.Split(',')[0]);
                        }
                    }
                }

                V4Orgs.Add("https://dev-v4-admin.sharevision.ca");
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }



        static string GetConnectionString(string siteUrl)
        {
            string connectionString = "";
            try
            {
                //use TenentContext to get ConnectionString

                string spHostName = "";

                spHostName = siteUrl.Replace("https://", "");
                //string webUrl = SPContext.Current.Web.Url.ToLower();
                

                // Provide the query string with a parameter placeholder.
                string queryString =
                    "SELECT Connection from dbo.tenant "
                        + "WHERE SPHost = @host "
                        + ";";

                // Create and open the connection in a using block. This
                // ensures that all resources will be closed and disposed
                // when the code exits.
                String TenentConectionString = ConfigurationManager.ConnectionStrings["TenantContext"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(TenentConectionString))
                {
                    // Create the Command and Parameter objects.
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue("@host", spHostName);

                    // Open the connection in a try/catch block. 
                    // Create and execute the DataReader, writing the result
                    // set to the console window.
                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            connectionString = reader[0].ToString();
                        }
                        reader.Close();
                    }
                    catch (Exception ex)
                    {
                        LogError(ex);
                    }

                }


            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return connectionString;
        }
        static void LogError(Exception ex)
        {
            logger.Error(ex.Message);
            logger.Error(ex.StackTrace);
        }
    }
}
