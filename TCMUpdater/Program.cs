﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;

namespace TCMUpdater
{
    class Program
    {
        static log4net.ILog logger = null;
        static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                logger = log4net.LogManager.GetLogger("File");
                logger.Info("Starting TCM Updater");

                UpdateTCMLinks();

                logger.Info("Done--------------------------------------");
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        static void LogError(Exception ex)
        {
            logger.Error(ex.Message);
            logger.Error(ex.StackTrace);
        }

        static void UpdateTCMLinks()
        {
            int count = 0;
            ArrayList URLsToAvoid = new ArrayList();
            //V4 server
            URLsToAvoid.Add("http://v4dev");
            URLsToAvoid.Add("http://v4dev:36306");
            URLsToAvoid.Add("http://v4dev:36306/sites/Help");
            URLsToAvoid.Add("https://sas.sharevision.ca");
            URLsToAvoid.Add("http://vserver14760:38673");
            URLsToAvoid.Add("http://vserver14760:38673/sites/Help");
            URLsToAvoid.Add("https://testsite2013.sharevision.ca");
            SPFarm farm = SPFarm.Local;
            foreach (SPService objService in farm.Services)
            {
                if (objService is SPWebService)
                {
                    SPWebService oWebService = (SPWebService)objService;
                    foreach (SPWebApplication webApp in oWebService.WebApplications)
                    {
                        SPSiteCollection siteCollections = webApp.Sites;
                        foreach (SPSite siteCollection in siteCollections)
                        {
                            if (URLsToAvoid.Contains(siteCollection.Url))
                                continue;
                            SPWeb oWeb = siteCollection.OpenWeb();
                            SPList list = null;
                            try
                            {
                                string path = "";
                                string SiteUrl = oWeb.Url;
                                int thirdSlash = SiteUrl.IndexOf("/", 8);
                                if (thirdSlash > 0)  //Non-Subsites will such as Inclusionpowellriver will not have a 3rd slash, and this variable will be -1.
                                    path = SiteUrl.Substring(thirdSlash);

                                list = oWeb.GetList(path + "/lists/TrainingCourseSections/"); //If the list does not exist at this url, an exception is thrown.
                            }
                            catch (Exception ee)
                            {
                                list = null;
                            }

                            logger.Info("Processing " + oWeb.Url);

                            if (list != null)
                            {

                                //update TCM LINKs
                                SPQuery query = new SPQuery();
                                query.Query = "<Where><Contains><FieldRef Name='Content' /><Value Type='Text'>/Portals+Pages+and+Page+Parts+Creating+Editing+and+Deleting</Value></Contains></Where>";

                                SPListItemCollection items = list.GetItems(query);
                                logger.Info("Found " + items.Count + " items");

                                foreach (SPListItem item in items)
                                {


                                    string html = item["Content"] + "";
                                    //logger.Info("HTML BEFORE");
                                    //logger.Info(html);

                                    html = html.Replace("<li><span style=\"text-decoration&#58;underline;\"><span style=\"color&#58;#0000ff;text-decoration&#58;underline;\"><strong><a style=\"color&#58;#0000ff;text-decoration&#58;underline;\" href=\"https&#58;//sharevision.atlassian.net/wiki/spaces/SVKB/pages/44061664/Portals+Pages+and+Page+Parts+Creating+Editing+and+Deleting\" target=\"_blank\"><span style=\"font-family&#58;-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Noto Sans', Ubuntu, 'Droid Sans', 'Helvetica Neue', sans-serif;\"><span style=\"font-size&#58;18.6667px;letter-spacing&#58;-0.186667px;\">Portals contain pages. </span></span></a></strong></span></span></li>",

"<li><span style=\"text-decoration&#58;underline;\"><span style=\"color&#58;#0000ff;text-decoration&#58;underline;\"><strong><a style=\"color&#58;#0000ff;text-decoration&#58;underline;\" href=\"https&#58;//sharevision.atlassian.net/wiki/spaces/SVKB/pages/2033287873/Portals+Set+Up+and+Configuration\" target=\"_blank\"><span style=\"font-family&#58;-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Noto Sans', Ubuntu, 'Droid Sans', 'Helvetica Neue', sans-serif;\"><span style=\"font-size&#58;18.6667px;letter-spacing&#58;-0.186667px;\">Portals contain pages. </span></span></a></strong></span></span></li>");



                                    html = html.Replace("<li><span style=\"text-decoration&#58;underline;\"><span style=\"color&#58;#0000ff;text-decoration&#58;underline;\"><strong><a style=\"color&#58;#0000ff;text-decoration&#58;underline;\" href=\"https&#58;//sharevision.atlassian.net/wiki/spaces/SVKB/pages/44061664/Portals+Pages+and+Page+Parts+Creating+Editing+and+Deleting\" target=\"_blank\"><span style=\"font-family&#58;-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Noto Sans', Ubuntu, 'Droid Sans', 'Helvetica Neue', sans-serif;\"><span style=\"font-size&#58;18.6667px;letter-spacing&#58;-0.186667px;\">Pages contain page parts. </span></span></a></strong></span></span></li>",


                                        "<li><span style=\"text-decoration&#58;underline;\"><span style=\"color&#58;#0000ff;text-decoration&#58;underline;\"><strong><a style=\"color&#58;#0000ff;text-decoration&#58;underline;\" href=\"https&#58;//sharevision.atlassian.net/wiki/spaces/SVKB/pages/2034630657/Pages+Set+Up+and+Configuration\" target=\"_blank\"><span style=\"font-family&#58;-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Noto Sans', Ubuntu, 'Droid Sans', 'Helvetica Neue', sans-serif;\"><span style=\"font-size&#58;18.6667px;letter-spacing&#58;-0.186667px;\">Pages contain page parts. </span></span></a></strong></span></span></li>");



                                    html = html.Replace("<li><span style=\"color&#58;#0000ff;\"><strong><a style=\"color&#58;#0000ff;\" href=\"https&#58;//sharevision.atlassian.net/wiki/spaces/SVKB/pages/44061664/Portals+Pages+and+Page+Parts+Creating+Editing+and+Deleting\" target=\"_blank\"><span style=\"font-family&#58;-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Noto Sans', Ubuntu, 'Droid Sans', 'Helvetica Neue', sans-serif;\"><span style=\"font-size&#58;18.6667px;letter-spacing&#58;-0.186667px;\"><span style=\"text-decoration&#58;underline;\">Page parts display data/information</span>&#160;</span></span></a></strong></span></li>",

                                        "<li><span style=\"color&#58;#0000ff;\"><strong><a style=\"color&#58;#0000ff;\" href=\"https&#58;//sharevision.atlassian.net/wiki/spaces/SVKB/pages/2009830871/PageParts+Set+Up+and+Configuration\" target=\"_blank\"><span style=\"font-family&#58;-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Noto Sans', Ubuntu, 'Droid Sans', 'Helvetica Neue', sans-serif;\"><span style=\"font-size&#58;18.6667px;letter-spacing&#58;-0.186667px;\"><span style=\"text-decoration&#58;underline;\">Page parts display data/information</span>&#160;</span></span></a></strong></span></li>");


                                    item["Content"] = html;
                                    item.SystemUpdate(false);

                                    count++;
                                    //return;
                                    //logger.Info("HTML AFTER");
                                    //logger.Info(html);
                                }

                            }
                            else
                            {
                                logger.Info("TCM list is not found");
                            }

                        }
                    }
                }
            }

            logger.Info("updated " + count + " sites");
        }

        static void UpdateTCM()
        {
            ArrayList URLsToAvoid = new ArrayList();
            //V4 server
            URLsToAvoid.Add("http://v4dev");
            URLsToAvoid.Add("http://v4dev:36306");
            URLsToAvoid.Add("http://v4dev:36306/sites/Help");
            URLsToAvoid.Add("https://sas.sharevision.ca");
            URLsToAvoid.Add("http://vserver14760:38673");
            URLsToAvoid.Add("http://vserver14760:38673/sites/Help");
            URLsToAvoid.Add("https://testsite2013.sharevision.ca");
            SPFarm farm = SPFarm.Local;
            foreach (SPService objService in farm.Services)
            {
                if (objService is SPWebService)
                {
                    SPWebService oWebService = (SPWebService)objService;
                    foreach (SPWebApplication webApp in oWebService.WebApplications)
                    {
                        SPSiteCollection siteCollections = webApp.Sites;
                        foreach (SPSite siteCollection in siteCollections)
                        {
                            if (URLsToAvoid.Contains(siteCollection.Url))
                                continue;
                            SPWeb oWeb = siteCollection.OpenWeb();
                            SPList list = null;
                            try
                            {
                                string path = "";
                                string SiteUrl = oWeb.Url;
                                int thirdSlash = SiteUrl.IndexOf("/", 8);
                                if (thirdSlash > 0)  //Non-Subsites will such as Inclusionpowellriver will not have a 3rd slash, and this variable will be -1.
                                    path = SiteUrl.Substring(thirdSlash);

                                list = oWeb.GetList(path + "/lists/TrainingCourseConfig"); //If the list does not exist at this url, an exception is thrown.
                            }
                            catch (Exception ee)
                            {
                                list = null;
                            }

                            logger.Info("Processing " + oWeb.Url);

                            if (list != null)
                            {
                                //PassPercentage
                                logger.Info("TCM list has " + list.ItemCount + " items");
                                //figure out how many has null value in PassPercentage
                                SPQuery query = new SPQuery();
                                query.Query = "<Where><IsNull><FieldRef Name='PassPercentage' /></IsNull></Where>";
                                SPListItemCollection items = list.GetItems(query);
                                logger.Info("TCM list has " + items.Count + " items with no PassPercentage value");

                                SPField fld = list.Fields.GetField("PassPercentage");
                                //update TCM items
                                foreach (SPListItem item in items)
                                {
                                    //string courseName = item["CourseName"] + "";
                                    //logger.Info(courseName);
                                    item[fld.Id] = 0;
                                    item.Update();
                                }

                                //update field
                                fld.Required = true;
                                fld.Update();

                                
                            }
                            else
                            {
                                logger.Info("TCM list is not found");
                            }

                        }
                    }
                }
            }
        }
    }
}
