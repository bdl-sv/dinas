﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using OfficeOpenXml;
using System.IO; 

namespace ListAllUsers
{
    class Program
    {
        static log4net.ILog logger = null;
        static List<string> URLsToAvoid = new List<string>();
        static List<string> BDLUsers = new List<string>();
        static string DefaultFileName = "Users.xlsx";
        static ExcelPackage pck = null;
        static ExcelWorksheet ws = null;
        static int RowNum = 1;
       

        static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                logger = log4net.LogManager.GetLogger("File");
                logger.Info("Starting");

                BDLUsers.Add("dina");
                BDLUsers.Add("alex.s");
                BDLUsers.Add("asraful");
                BDLUsers.Add("ben");
                BDLUsers.Add("breakwater");
                BDLUsers.Add("brittany.h");
                BDLUsers.Add("dion");
                BDLUsers.Add("donlr");
                BDLUsers.Add("jamie");
                BDLUsers.Add("justin");
                BDLUsers.Add("kathleen");
                BDLUsers.Add("kristen.finnsson");
                BDLUsers.Add("donlr");
                BDLUsers.Add("laurie");
                BDLUsers.Add("lucas");
                BDLUsers.Add("maksud");
                BDLUsers.Add("mike");
                BDLUsers.Add("philklassen");
                BDLUsers.Add("rajib");
                BDLUsers.Add("ronb");
                BDLUsers.Add("wahl");
                BDLUsers.Add("dbaccess");
                BDLUsers.Add("sv.expert");
                BDLUsers.Add("sv.admin");
                BDLUsers.Add("sv.staff");
                BDLUsers.Add("marty");
                BDLUsers.Add("nazmul");
                BDLUsers.Add("steve");
                BDLUsers.Add("sven");
                BDLUsers.Add("macy");

                pck = new ExcelPackage();
                ws = pck.Workbook.Worksheets.Add("UserList");

                ws.Cells[RowNum, 1].Value = "Login";
                ws.Cells[RowNum, 1].Style.Font.Bold = true;

                ws.Cells[RowNum, 2].Value = "Name";
                ws.Cells[RowNum, 2].Style.Font.Bold = true;

                ws.Cells[RowNum, 3].Value = "Email";
                ws.Cells[RowNum, 3].Style.Font.Bold = true;

                ws.Cells[RowNum, 4].Value = "Site";
                ws.Cells[RowNum, 4].Style.Font.Bold = true;

                ListSites();

                logger.Info("Done--------------------------------------");
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            finally
            {
                if (pck != null)
                {
                    pck.Dispose();
                }
            }
        }

        static void ListSites()
        {
            SPFarm farm = SPFarm.Local;
            foreach (SPService objService in farm.Services)
            {
                if (objService is SPWebService)
                {
                    SPWebService oWebService = (SPWebService)objService;
                    foreach (SPWebApplication webApp in oWebService.WebApplications)
                    {
                        SPSiteCollection siteCollections = webApp.Sites;
                        foreach (SPSite siteCollection in siteCollections)
                        {
                            if (URLsToAvoid.Contains(siteCollection.Url))
                                continue;

                            SiteStats(siteCollection);

                        }
                    }
                }
            }


            byte[] data = pck.GetAsByteArray();
            string path = @"C:\temp\AllUserList.xlsx";
            File.WriteAllBytes(path, data);

        }



        static void SiteStats(SPSite site)
        {
            try
            {
                SPWeb web = site.OpenWeb();

                logger.Info("====================================================================");
                logger.Info(web.Title);
                logger.Info(web.Url);

                logger.Info("Site Users " + web.SiteUsers.Count);
                logger.Info("SCA Users " + web.SiteAdministrators.Count);

               

                foreach (SPUser user in web.SiteUsers)
                {
                    string login = user.LoginName.Split('\\')[1];

                    if (BDLUsers.Contains(login) == false)
                    {
                        logger.Info(user.LoginName + " - " + user.Name + " - " + user.Email);
                        RowNum++;


                        ws.Cells[RowNum, 1].Value = user.LoginName;
                        ws.Cells[RowNum, 2].Value = user.Name;
                        ws.Cells[RowNum, 3].Value = user.Email;
                        ws.Cells[RowNum, 4].Value = web.Url;
                    }
                }

               
               
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        static void LogError(Exception ex)
        {
            logger.Error(ex.ToString());
        }
    }
}
