﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;

namespace QSUpdater
{
    class Program
    {
        static log4net.ILog logger = null;
        static List<string> sitesToAvoid = new List<string>();
        static List<string> faIcons = new List<string>();
        static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                logger = log4net.LogManager.GetLogger("File");
                logger.Info("Starting QS Updater");
                //sitesToAvoid.Add("m-caringtouch.sharevision.ca");
                //sitesToAvoid.Add("caringtouch.sharevision.ca");
                //sitesToAvoid.Add("vicc.sharevision.ca");
                //sitesToAvoid.Add("m-vicc.sharevision.ca");
                //sitesToAvoid.Add("ws.sharevision.ca");
                //sitesToAvoid.Add("m-ws.sharevision.ca");
                //sitesToAvoid.Add("parklandclass.sharevision.ca");
                //sitesToAvoid.Add("m-parklandclass.sharevision.ca");
                //sitesToAvoid.Add("qcla4.sharevision.ca");
                //sitesToAvoid.Add("m-qcla4.sharevision.ca");
                //sitesToAvoid.Add("mviss.sharevision.ca");
                //sitesToAvoid.Add("m-mviss.sharevision.ca");
                //sitesToAvoid.Add("clc2.sharevision.ca");
                //sitesToAvoid.Add("m-clc2.sharevision.ca");
                //sitesToAvoid.Add("dengarry.sharevision.ca");
                //sitesToAvoid.Add("m-dengarry.sharevision.ca");
                //sitesToAvoid.Add("axisfamily.sharevision.ca");
                //sitesToAvoid.Add("m-axisfamily.sharevision.ca");
                // axisfamily.sharevision.ca



                faIcons = System.IO.File.ReadLines(@"C:\inetpub\wwwroot\production1\fa-icons.txt").ToList();

                UpdateQS(); 

                logger.Info("Done--------------------------------------");
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        static void UpdateQS()
        {
            string conn = ConfigurationManager.ConnectionStrings["TenantContext"].ConnectionString;
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(conn))
            {
                connection.Open();
                System.Data.SqlClient.SqlCommand command;
                System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                String sql = "SELECT * FROM [dbo].[Tenant] ORDER BY v4Host";
                command = new System.Data.SqlClient.SqlCommand(sql, connection);
                adapter.SelectCommand = command;
                System.Data.DataSet ds = new System.Data.DataSet();
                adapter.Fill(ds, "Tenant");

                foreach (System.Data.DataTable table in ds.Tables)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        string connx = row["Connection"] +"";
                        string v4Host = row["v4Host"] + "";
                        string spHost = row["SpHost"] + "";

                        if (sitesToAvoid.Contains(v4Host))
                            continue;

                        logger.Info("v4Host " + v4Host + " - spHost " + spHost);

                        //if(v4Host == "testingv4.sharevision.ca")
                            UpdateSiteIcons(connx);
                    }
                }

 
                command.Dispose();
                adapter.Dispose();

            }
        }

        static void UpdateSiteIcons(string connectionString)
        {
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(connectionString))
            {
                connection.Open();
                System.Data.SqlClient.SqlCommand selectCommand = new System.Data.SqlClient.SqlCommand("SELECT * FROM [dbo].[Menu]", connection);
                System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                adapter.SelectCommand = selectCommand;
                System.Data.DataTable ds = new System.Data.DataTable();
                adapter.Fill(ds);

                foreach (DataRow row in ds.Rows)
                {
                    string id = row["ID"] + "";
                    string icon = row["Icon"] + "";

                    if (icon.Contains("fa ")) 
                    {
                        string[] bits = icon.Split(new string[] { "fa-" }, StringSplitOptions.None);
                        if (bits.Length == 2)
                        {
                            string temp = "fa-" + bits[1];

                            string newIcon = getNewIcon(temp);
                            if (String.IsNullOrEmpty(newIcon))
                            {
                                newIcon = icon.Replace("fa ", "");
                                if (newIcon.Contains("far ") == false && newIcon.Contains("fas ") == false && newIcon.Contains("fal ") == false && newIcon.Contains("fad ") == false && newIcon.Contains("fab ") == false)
                                    newIcon = "fa " + temp;
                            }
                            
                            logger.Info("ID " + id + " - Icon " + icon + " - new " + newIcon);


                            row["Icon"] = newIcon;
                        }
                        
                        
                    }

                    
                }

                System.Data.SqlClient.SqlCommand updateCommand = new System.Data.SqlClient.SqlCommand("UPDATE [dbo].[Menu] SET Icon = @Icon WHERE ID = @ID", connection);
                updateCommand.Parameters.Add("@Icon", SqlDbType.VarChar, 500, "Icon");
                updateCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
                adapter.UpdateCommand = updateCommand;
                adapter.Update(ds); 
                selectCommand.Dispose();
                adapter.Dispose();
            }
        }

        static string getNewIcon(string iconPart)
        {
            string newIcon = "";
            foreach (string s in faIcons)
            {
                if (s.EndsWith(iconPart))
                {
                    newIcon = s;
                    break;
                }
            }
            return newIcon;
        }

        static void LogError(Exception ex)
        {
            logger.Error(ex.Message);
            logger.Error(ex.StackTrace);
        }

    }
}
