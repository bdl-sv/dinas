﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;


namespace ResetEAM
{
    class Program
    {
        static log4net.ILog logger = null;

        static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                logger = log4net.LogManager.GetLogger("File");
                logger.Info("Starting Reset Email Logs");


                UpdateAllSites();

                logger.Info("DONE!");
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }


        private static void UpdateAllSites()
        {
            ArrayList URLsToAvoid = new ArrayList();
            //All our Server Farms have non-SharePoint sites or SharePoint Sites that are not client Sites.  No need doing any work on them.

            //9625
            URLsToAvoid.Add("https://reports2010.sharevision.ca");
            URLsToAvoid.Add("http://wiki.sharevision.ca");
            URLsToAvoid.Add("http://server9625:6754");
            URLsToAvoid.Add("http://server9625:6754/sites/Help");

            //8525
            URLsToAvoid.Add("http://server8525:36136");
            URLsToAvoid.Add("http://ssp8525.sharevision.ca/ssp/admin");
            URLsToAvoid.Add("http://reports8525.sharevision.ca");
            URLsToAvoid.Add("http://bamboossp.sharevision.ca/bssp/admin");
            URLsToAvoid.Add("http://cert8525.sharevision.ca");
            URLsToAvoid.Add("http://www.sharevision.ca");
            URLsToAvoid.Add("http://voith.sonar.ca");
            URLsToAvoid.Add("http://test2010.sharevision.ca");
            URLsToAvoid.Add("https://venturereports.sharevision.ca");
            URLsToAvoid.Add("http://diversitydialogue.sharevision.ca");
            URLsToAvoid.Add("http://community.sharevision.ca");
            URLsToAvoid.Add("http://v3.svenbrindley.com");
            URLsToAvoid.Add("http://untape.sharevision.ca");

            //8693
            URLsToAvoid.Add("http://server8693:44117");
            URLsToAvoid.Add("http://reports8693.sharevision.ca");
            URLsToAvoid.Add("http://ssp8693.sharevision.ca/ssp/admin");
            URLsToAvoid.Add("https://cert8693.sharevision.ca");
            URLsToAvoid.Add("http://www.clanbc.ca");
            URLsToAvoid.Add("https://spectrumreports.sharevision.ca");
            URLsToAvoid.Add("https://aimhiv3.sharevision.ca");
            URLsToAvoid.Add("https://j.sharevision.ca");

            //16765
            URLsToAvoid.Add("https://esreports.sharevision.ca");
            URLsToAvoid.Add("https://reports16765.sharevision.ca");
            URLsToAvoid.Add("https://essentials.sharevision.ca");
            URLsToAvoid.Add("http://server9889:30155");
            URLsToAvoid.Add("http://server9889:30155/sites/Help");
            URLsToAvoid.Add("http://server16765:30155");
            URLsToAvoid.Add("http://server16765:30155/sites/Help");

            SPFarm farm = SPFarm.Local;

            foreach (SPService objService in farm.Services)
            {
                if (objService is SPWebService)
                {
                    SPWebService oWebService = (SPWebService)objService;
                    foreach (SPWebApplication webApp in oWebService.WebApplications)
                    {
                        SPSiteCollection siteCollections = webApp.Sites;
                        foreach (SPSite siteCollection in siteCollections)
                        {
                            SPWeb oWeb = null;
                            try
                            {
                                if (URLsToAvoid.Contains(siteCollection.Url))
                                    continue;
                                oWeb = siteCollection.OpenWeb();

                                SPList list = null;
                                string path = "";
                                string SiteUrl = oWeb.Url;
                                int thirdSlash = SiteUrl.IndexOf("/", 8);
                                if (thirdSlash > 0)  //Non-Subsites will such as Inclusionpowellriver will not have a 3rd slash, and this variable will be -1.
                                    path = SiteUrl.Substring(thirdSlash);

                                list = oWeb.GetList(path + "/lists/configemailalertslog"); //If the list does not exist at this url, an exception is thrown.

                                if (list != null && list.Fields.ContainsField("EMailDeliveryStatus"))
                                {
                                    //reset email for the last 3 days

                                    SPQuery query = new SPQuery();
                                    query.Query = "<Where><And><Eq><FieldRef Name='EMailDeliveryStatus' /><Value Type='Text'>Success</Value></Eq>"
                                        + "<Geq><FieldRef Name='Created' /><Value Type='DateTime'><Today OffsetDays='-3' /></Value></Geq></And></Where>";

                                    SPListItemCollection items = list.GetItems(query);
                                    logger.Info("Site: " + oWeb.Url + " - items to update " + items.Count);

                                    foreach (SPListItem item in items)
                                    {
                                        item["EMailDeliveryStatus"] = "Failure, resending";
                                        item.Update();
                                    }

                                }
                            }
                            catch (Exception ex)
                            {
                                //LogError(ex);
                            }
                        }
                    }
                }
            }
        }

        static void LogError(Exception ex)
        {
            logger.Error(ex.Message);
            logger.Error(ex.ToString());

        }
    }
}
