﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Reflection;
using Microsoft.SharePoint;
using ArtfulBits.SiteColumns;
using System.IO;
using System.Drawing;
using Microsoft.SharePoint.Utilities;

namespace ImportImages
{
    class Program
    {
        static log4net.ILog logger = null;
        private static readonly char[] c_achSlashes = new char[2]
        {
            '\\',
            '/'
        };
        private static string[] c_arrArchiveExtensions = new string[6]
        {
            "zip",
            "rar",
            "7z",
            "bzip2",
            "gzip",
            "tar"
        };

        static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                logger = log4net.LogManager.GetLogger("File");
                logger.Info("Starting Import Images");

                ImportImages();


                logger.Info("Done--------------------------------------");
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }

        static void ImportImages()
        {
            string spSiteUrl = ConfigurationManager.AppSettings["SPSiteUrl"];

            using (SPSite site = new SPSite(spSiteUrl))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    SPQuery query = new SPQuery();
                    //query.Query = "<Where><IsNotNull><FieldRef Name='Photo' /></IsNotNull></Where>";
                    query.Query = "<Where><IsNotNull><FieldRef Name='Image' /></IsNotNull></Where>";
                    //query.Query = "<Where><Eq><FieldRef Name='ID' /><Value Type='Counter'>1603</Value></Eq></Where>";
                    SPListItemCollection items = web.Lists[new Guid("{459CFC7D-4D56-4529-AE1F-55ED7744FE83}")].GetItems(query);
                    SPList list = web.Lists[new Guid("{459CFC7D-4D56-4529-AE1F-55ED7744FE83}")];
                    //SPField spf = list.Fields.GetField("Picture");
                    SPField spf = list.Fields.GetField("Photo");

                    foreach (SPListItem item in items)
                    {
                        try
                        {
                            SPFieldUrlValue urlValue = new SPFieldUrlValue(item["Image"] + "");
                            SPFile file = web.GetFile(urlValue.Url);
                            string rawValue = urlValue.Url;


                            string subFolderName = Guid.NewGuid().ToString("N");
                            SPFolder imageFolder = GetSubFolder(list.RootFolder, "UploadImages", true);

                            long contentLength = file.Length;
                            Stream inputStream = file.OpenBinaryStream(SPOpenBinaryOptions.SkipVirusScan);
                            inputStream.Seek(0L, SeekOrigin.Begin);
                            byte[] array = ResizeLargeImagesIfNeeded(inputStream, (ImageUploadField)spf);
                            if (array == null)
                            {
                                byte[] array2 = new byte[contentLength];
                                inputStream.Seek(0L, SeekOrigin.Begin);
                                if (inputStream.Read(array2, 0, (int)contentLength) == contentLength)
                                {
                                    array = array2;
                                }
                            }
                            SPFolder uploadFolder = GetUploadFolder(list, subFolderName, true);
                            string fileNameFromURL = GetFileNameFromURL(file.Name);
                            SPFile spfile = uploadFolder.Files.Add(fileNameFromURL, array, true);


                            if (spfile != null)
                            {
                                string text = CombineUrl(list.ParentWeb.ServerRelativeUrl, spfile.Url);
                                bool flag = false;
                                using (Stream stream = spfile.OpenBinaryStream())
                                {
                                    IUImage iUImage = null;
                                    try
                                    {
                                        iUImage = IUImage.FromStream(stream);
                                        flag = true;
                                    }
                                    catch
                                    {
                                    }
                                    if (flag)
                                    {
                                        if (((ImageUploadField)spf).ImageSizeType != 0)
                                        {
                                            EnsureThumbnail(iUImage, spfile, (ImageUploadField)spf, list.ParentWeb);
                                        }
                                        iUImage.Dispose();
                                    }
                                }
                                SPFieldMultiColumnValue sPFieldMultiColumnValue = new SPFieldMultiColumnValue(4);
                                sPFieldMultiColumnValue[0] = text;
                                sPFieldMultiColumnValue[1] = "imported from pictrue library";
                                if (list.EnableAttachments)
                                {
                                    sPFieldMultiColumnValue[2] = "new";
                                }
                                sPFieldMultiColumnValue[3] = (flag ? "" : GetIconFileName(list.ParentWeb, text, IconSize.Size32));
                                //return sPFieldMultiColumnValue;
                                item[spf.Id] = sPFieldMultiColumnValue;
                                item.Update();

                                logger.Info("Imported image for item " + item.ID + " - " + text);
                                System.Threading.Thread.Sleep(500);
                            }




                        }
                        catch (Exception ex)
                        {
                            LogError(ex);
                        }
                    }

                }
            }
        }

        static string GetFileExtension(string fileName)
        {
            string result = "";
            if (!string.IsNullOrEmpty(fileName))
            {
                int num = fileName.LastIndexOf('.') + 1;
                if (0 < num && num < fileName.Length)
                {
                    result = fileName.Substring(num).ToLowerInvariant();
                }
            }
            return result;
        }
        private static bool IsArchiveFile(string strFileName)
        {
            string fileExtension = GetFileExtension(strFileName);
            return -1 != Array.IndexOf(c_arrArchiveExtensions, fileExtension);
        }
        static string GetIconFileName(SPWeb web, string strFileName, IconSize size)
        {
            string text = "";
            if (IsArchiveFile(strFileName))
            {
                return "ArtfulBits.SiteColumns/ImageUpload/Icons/zip.gif";
            }
            return SPUtility.MapToIcon(web, strFileName, "", size);
        }

        public static SPFile EnsureThumbnail(IUImage imageOrig, SPFile fileOrig, ImageUploadField imageUpload, SPWeb web)
        {
            SPFile result = null;
            Size thumbnailRestriction = imageUpload.GetThumbnailRestriction();
            Size thumbnailDimensions = GetThumbnailDimensions(imageOrig.Size, thumbnailRestriction);
            if (0 < thumbnailDimensions.Width || 0 < thumbnailDimensions.Height)
            {
                SPFolder parentFolder = fileOrig.ParentFolder;
                parentFolder.ParentWeb.AllowUnsafeUpdates = true;
                SPFolder thumbnailFolder = GetThumbnailFolder(parentFolder, thumbnailDimensions, true);
                string strUrl = EnsureEndsWithPathSeparator(thumbnailFolder.ServerRelativeUrl) + fileOrig.Name;
                SPFile sPFile = null;
                try
                {
                    sPFile = (web.GetFileOrFolderObject(strUrl) as SPFile);
                }
                catch
                {
                }
                if (sPFile == null)
                {
                    imageOrig.ResizeImage(thumbnailDimensions, true);
                    using (Stream file = imageOrig.ToStream())
                    {
                        sPFile = thumbnailFolder.Files.Add(fileOrig.Name, file, true);
                    }
                }
                result = sPFile;
            }
            return result;
        }
        public static string EnsureEndsWithPathSeparator(string url)
        {
            return (string.IsNullOrEmpty(url) || !url.EndsWith("/", StringComparison.OrdinalIgnoreCase)) ? (url + "/") : url;
        }
        public static SPFolder GetThumbnailFolder(SPFolder folderPar, Size size, bool ensureToCreate)
        {
            string folderName = $"t_{((0 < size.Width) ? size.Width : 0)}_{((0 < size.Height) ? size.Height : 0)}";
            return GetSubFolder(folderPar, folderName, ensureToCreate);
        }
        public static string CombineUrl(string baseUrlPath, string url)
        {
            if (string.IsNullOrEmpty(baseUrlPath))
            {
                baseUrlPath = "";
            }
            if (string.IsNullOrEmpty(url))
            {
                url = "";
            }
            bool flag = baseUrlPath.EndsWith("/");
            bool flag2 = url.StartsWith("/");
            string text = "";
            if (flag && flag2)
            {
                return baseUrlPath + url.Substring(1);
            }
            if (!flag && flag2)
            {
                goto IL_006d;
            }
            if (flag && !flag2)
            {
                goto IL_006d;
            }
            return baseUrlPath + "/" + url;
        IL_006d:
            return baseUrlPath + url;
        }


        public static string GetFileNameFromURL(string path)
        {
            string result = "";
            if (!string.IsNullOrEmpty(path))
            {
                int num = path.LastIndexOfAny(c_achSlashes);
                result = ((num > 0) ? path.Substring(num + 1) : path);
            }
            return result;
        }
        private static SPFolder GetUploadFolder(SPList list, string subFolderName, bool ensureToCreate)
        {
            SPFolder result = null;
            SPFolder uploadRootFolder = GetUploadRootFolder(list, ensureToCreate);
            if (uploadRootFolder != null)
            {
                result = GetSubFolder(uploadRootFolder, subFolderName, ensureToCreate);
            }
            return result;
        }

        private static SPFolder GetUploadRootFolder(SPList list, bool ensureToCreate)
        {
            return GetSubFolder(list.RootFolder, "UploadImages", ensureToCreate);
        }


        private static byte[] ResizeLargeImagesIfNeeded(Stream stream, ImageUploadField field)
        {
            byte[] bytesImage = null;
            Size maxSize = new Size(field.MaxWidth, field.MaxHeight);
            if (0 < maxSize.Width || 0 < maxSize.Height)
            {

                using (IUImage iUImage = IUImage.FromStream(stream))
                {
                    Size thumbnailDimensions = GetThumbnailDimensions(iUImage.Size, maxSize);
                    if (thumbnailDimensions.Width < iUImage.Size.Width || thumbnailDimensions.Height < iUImage.Size.Height)
                    {
                        iUImage.ResizeImage(thumbnailDimensions, false);
                        bytesImage = iUImage.ToBytes();
                    }
                }

            }
            return bytesImage;
        }

        public static Size GetThumbnailDimensions(Size imageSize, Size maxSize)
        {
            float num = (float)imageSize.Width;
            float num2 = (float)imageSize.Height;
            if (0 < maxSize.Width && (float)maxSize.Width < num)
            {
                num = (float)maxSize.Width;
                num2 = (float)(imageSize.Height * maxSize.Width / imageSize.Width);
            }
            if (0 < maxSize.Height && (float)maxSize.Height < num2)
            {
                num = (float)(imageSize.Width * maxSize.Height / imageSize.Height);
                num2 = (float)maxSize.Height;
            }
            return Size.Ceiling(new SizeF(num, num2));
        }

        static SPFolder GetSubFolder(SPFolder folderParent, string folderName, bool ensureToCreate)
        {
            SPFolder sPFolder = null;
            try
            {
                sPFolder = folderParent.SubFolders[folderName];
            }
            catch (Exception)
            {
            }
            if (sPFolder == null && ensureToCreate)
            {
                sPFolder = folderParent.SubFolders.Add(folderName);
            }
            return sPFolder;
        }




        static void LogError(Exception ex)
        {
            logger.Error(ex.Message);
            logger.Error(ex.StackTrace);
        }
    }
}
