﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using Microsoft.SharePoint;
using OfficeOpenXml;
using System.Diagnostics;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;
using System.Data.SqlClient;

namespace McManIndividualArchiverAndDeletionTool
{
    class Program
    {
        static log4net.ILog logger = null;
        static string DefaultFileName = "DataExport.xlsx";
        static string DefaultZipFileName = "MyDocuments.zip";
        static string DefaultFileNameContactNotes = "ContactNotesDataExport.xlsx";
        static string ExportedDataLibName = "BulkIndividualsDataExport";
        static string ArchivingAndDeletionToolConfigurationListGuid = "{74BCEC55-D4A0-4D05-93E4-12E3540A9B06}";
        static List<string> processedIndividuals = new List<string>();
        static List<string> workbookNames = new List<string>();
        static uint MaxIndividualsPerRun = 25;
        static string dateFormat;
        static string timeFormat;
        static string connectionString;
        static string ACTIVITY_LIST_ID = "{DBC75B30-A277-4161-B7E5-853DCE030A28}";
        static string SUBACTIVITY_LIST_ID = "{DED0E606-B15E-4FA8-B870-D8AB573F0051}";
        static SPList serviceList;
        static SPList actList;
        static SPList subActList;

        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            logger = log4net.LogManager.GetLogger("File");
            logger.Info("Starting Feature MCED-320: Archive and Delete Inactive Individuals.");
            string spSiteUrl = ConfigurationManager.AppSettings["SPSiteUrl"];
            connectionString = ConfigurationManager.ConnectionStrings["V4Context"].ConnectionString;

            using (SPSite site = new SPSite(spSiteUrl))
            {
                using (SPWeb web = site.OpenWeb())
                {                    
                    SPListItemCollection InactiveIndividualsSet = GetIndividuals(web);
                    if (InactiveIndividualsSet == null)
                    {
                        logger.Info("There was an issue getting Individuals.  No achiving performed.");
                        return;
                    }
                    else if (InactiveIndividualsSet.Count == 0)
                    {
                        logger.Info("No Individuals scheduled.");
                        return;
                    }

                    dateFormat = GetDateFormat(web);
                    timeFormat = GetTimeFormat(web);

                    SPList serviceList = web.Lists[new Guid("{459CFC7D-4D56-4529-AE1F-55ED7744FE83}")];
                    SPList actList = web.Lists[new Guid(ACTIVITY_LIST_ID)];
                    SPList subActList = web.Lists[new Guid(SUBACTIVITY_LIST_ID)];

                    //We have some work to do!
                    //Because we're going to be accessing lists many times depending on the number of Individuals Picked, let's only find the lists once.
                    List<SPList> IndividualLists = new List<SPList>();
                    SPListCollection lists = web.Lists;

                    for (int x = 0; x < lists.Count; x++)// (SPList list in lists)
                    {
                        SPList list = lists[x];
                        string listName = "";
                        try
                        {
                            listName = list.Fields[0].Title; //This will ensure we have a working list.
                            listName = list.Title;
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }

                        if (list.ID.ToString().ToUpper() == "9932DFEF-4CD5-4777-AAAA-060191FEB3AB" || list.ID.ToString().ToUpper() == "74BCEC55-D4A0-4D05-93E4-12E3540A9B06")
                            continue;

                        foreach (SPField oField in list.Fields)
                        {
                            if (oField.Reorderable)
                            {
                                if (oField.Type == SPFieldType.Lookup || oField.TypeDisplayName == "Filtered Lookup" || oField.TypeDisplayName == "Filtered Lookup field")
                                {
                                    SPFieldLookup oLookupField = (SPFieldLookup)oField;
                                    if (oLookupField.LookupList == "{703fd232-253a-4d00-bac8-cccf2367c36d}" && oLookupField.LookupField == "FullName")
                                    {
                                        IndividualLists.Add(list);
                                        break;
                                    }
                                }
                            }
                        }
                    }


                    SPList SelectedIndividualsList = web.Lists[new Guid(ArchivingAndDeletionToolConfigurationListGuid)];
                    Guid gIndividualColumnId = SelectedIndividualsList.Fields.GetFieldByInternalName("InactiveIndividual").Id;
                    Guid gStatusColumnId = SelectedIndividualsList.Fields.GetFieldByInternalName("Status").Id;
                    Guid gIndividualTextColumnId = SelectedIndividualsList.Fields.GetFieldByInternalName("IndividualText").Id;

                    foreach (SPListItem individualItem in InactiveIndividualsSet)
                    {
                        SPFieldLookupValue person = new SPFieldLookupValue(individualItem[gIndividualColumnId] + "");
                        try
                        {
                            logger.Info("Starting Export for: " + person.LookupValue);
                            ExportPerson(web, person, IndividualLists);
                            ZipIndDocs(web, person);
                            individualItem[gStatusColumnId] = "Success - Individual has been exported/archived.  Operation still in Progress: Deletion";
                            individualItem.Update();
                        }
                        catch(Exception ex)
                        {
                            logger.Error("Error in Exporting: " + person.LookupValue + " Error=" + ex.ToString());
                            individualItem[gStatusColumnId] = "Error in Exporting: " + ex.ToString();
                            individualItem.Update();
                            continue;
                        }

                        try
                        {   
                            DeletePerson(web, person, IndividualLists);
                            individualItem[gStatusColumnId] = "Success: Operation Completed";
                            individualItem[gIndividualTextColumnId] = person.LookupValue;
                            individualItem.Update();
                        }
                        catch (Exception ex)
                        {
                            logger.Error("Error in Deleeting data: " + person.LookupValue + " Error=" + ex.ToString());
                            individualItem[gStatusColumnId] = "Error in Removing Individual Items and Documents: " + ex.ToString();
                            individualItem.Update();
                            continue;
                        }
                    }
                }
            }            
        }

        static void ExportPerson(SPWeb web, SPFieldLookupValue person, List<SPList> IndividualLists)
        {
            workbookNames = new List<string>();
            Dictionary<string, string> attachments = new Dictionary<string, string>();

            string sFileAndPath = GetExcelPathAndFilename(web, person.LookupValue.Trim(), DefaultFileName);
            //See if this file is present
            SPFolder oIndFolder = null;
            SPFile oExport = null;
            bool bFolderExists = FolderExists(web.Url + "/" + ExportedDataLibName + "/" + EncodeName(person.LookupValue.Trim()), web, ref oIndFolder);
            if (bFolderExists == false)
            {
                CreateIndividualExportFolder(web, EncodeName(person.LookupValue.Trim()));
                FolderExists(web.Url + "/" + ExportedDataLibName + "/" + EncodeName(person.LookupValue.Trim()), web, ref oIndFolder);
            }

            bool bFileExists = FileExists(sFileAndPath, oIndFolder, ref oExport);
            ExcelPackage pck;

            if (bFileExists)
            {
                pck = new ExcelPackage(oExport.OpenBinaryStream());
            }
            else
            {
                pck = new ExcelPackage();
            }

            string safeListName = GetSafeListname("Attendance");
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add(safeListName);
            FillAttendance(ws, person, web);

            //They don't have this installed.
            //safeListName = GetSafeListname("Comm Log");
            //ws = pck.Workbook.Worksheets.Add(safeListName);
            //success = FillCommLog(ws, person, web);
            //if (!success)            
            //  throw (new Exception("Error with Communication Log Items"));

            for (int x = 0; x < IndividualLists.Count; x++)
            {
                SPList list = IndividualLists[x];

                string sQuery = "";

                foreach (SPField oField in list.Fields)
                {
                    if (oField.Reorderable)
                    {
                        if (oField.Type == SPFieldType.Lookup || oField.TypeDisplayName == "Filtered Lookup" || oField.TypeDisplayName == "Filtered Lookup field")
                        {
                            SPFieldLookup oLookupField = (SPFieldLookup)oField;
                            if (oLookupField.LookupList == "{703fd232-253a-4d00-bac8-cccf2367c36d}" && oLookupField.LookupField == "FullName")
                            {
                                string tempQ = "<Eq><FieldRef Name='" + oLookupField.InternalName + "' LookupId='true' /><Value Type='Lookup'>" + person.LookupId + "</Value></Eq>";

                                if (oField.Indexed == true)
                                {
                                    tempQ = "<Eq><FieldRef Name='" + oLookupField.InternalName + "' /><Value Type='Lookup'>" + person.LookupValue + "</Value></Eq>";
                                }

                                if (sQuery == "")
                                    sQuery = tempQ;
                                else
                                    sQuery = "<Or>" + tempQ + sQuery + "</Or>";
                            }
                        }
                    }
                }

                bool bHasContentTypesOn = list.ContentTypesEnabled;
                SPQuery Query2 = new SPQuery();
                Query2.Query = "<Where>" + sQuery + "</Where>";
                SPListItemCollection oListItems = list.GetItems(Query2);
                int rowCount = oListItems.Count;

                if (oListItems.Count > 0)
                {   
                    safeListName = GetSafeListname(list.Title);
                    //logger.Info("LIST: " + list.Title + " - WORKBOOK: " + safeListName);

                    ws = pck.Workbook.Worksheets.Add(safeListName);
                    //We want to Show some additional fields that are system fields.
                    ArrayList oExtraFieldInternalNames = new ArrayList();
                    if (bHasContentTypesOn)
                        oExtraFieldInternalNames.Add("ContentType");
                    oExtraFieldInternalNames.Add("Author");
                    oExtraFieldInternalNames.Add("Editor");
                    oExtraFieldInternalNames.Add("Created");
                    oExtraFieldInternalNames.Add("Modified");
                    oExtraFieldInternalNames.Add("ID");
                    //oExtraFieldInternalNames.Add("Attachments");

                    //Fill in the sheet! 
                    //Header First:
                    int iColumnNumber = 1;
                    int iRowNumber = 1;

                    foreach (SPField oField in list.Fields)
                    {
                        if (oField.Reorderable || oExtraFieldInternalNames.Contains(oField.InternalName))
                        {
                            ws.Cells[iRowNumber, iColumnNumber].Value = oField.Title;
                            ws.Cells[iRowNumber, iColumnNumber].Style.Font.Bold = true;
                            iColumnNumber++;
                        }
                    }

                    ws.Cells[iRowNumber, iColumnNumber].Value = "Attachments";
                    ws.Cells[iRowNumber, iColumnNumber].Style.Font.Bold = true;

                    foreach (SPListItem oItem in oListItems)
                    {
                        //Setup the Column and Row Numbers
                        iColumnNumber = 1;
                        iRowNumber++;

                        foreach (SPField oField in list.Fields)
                        {
                            if (oField.Reorderable || oExtraFieldInternalNames.Contains(oField.InternalName))
                            {
                                ws.Cells[iRowNumber, iColumnNumber].Value = SafelyGetTextFromColumn(oField, oItem);
                                iColumnNumber++;
                            }
                        }
                        string attValue = "";
                        foreach (string att in oItem.Attachments)
                        {

                            string attUrl = list.RootFolder.ServerRelativeUrl + "/Attachments/" + oItem.ID + "/" + att;
                            //SPFile attFile = web.GetFile(attUrl);

                            string attachmentName = "list_" + safeListName + "_item_" + oItem.ID + "_" + att;
                            attachments.Add(attachmentName, attUrl);
                            //attachmentFolder.Files.Add(attachmentName, attFile.OpenBinary(SPOpenBinaryOptions.SkipVirusScan));

                            attValue = attachmentName + "\n";
                        }
                        ws.Cells[iRowNumber, iColumnNumber].Value = attValue;

                        if (attValue != "")
                        {
                            //logger.Info("Found Attachments");
                        }
                    }                   
                    System.Threading.Thread.Sleep(250);
                }
            }

            // Save it, we're done!
            if (bFileExists == false)
            {
                if (oIndFolder == null)
                {
                    logger.Info("oIndFolder is null");
                }
                if (pck == null)
                {
                    logger.Info("pck is null");
                }
                oIndFolder.Files.Add(DefaultFileName, pck.GetAsByteArray());
                oIndFolder.Update();
            }
            else
            {
                oIndFolder.Files.Add(DefaultFileName, pck.GetAsByteArray(), true);
                oIndFolder.Update();
            }
            pck.Dispose();

            //zip attachments
            if (attachments.Count > 0)
            {
                string fileName = "e:\\Temp\\IndAttachments" + person.LookupId + "-" + DateTime.Now.ToString("yyyyMMddHHmm") + ".zip";

                using (ZipOutputStream zipStream = new ZipOutputStream(File.Create(fileName)))
                {
                    zipStream.SetLevel(9);
                    byte[] buffer = new byte[81920]; //Originally 4096, seems tiny!  Let's try 20 times that, 80kb.

                    foreach (KeyValuePair<string, string> keyValue in attachments)
                    {
                        ZipEntry newEntry = new ZipEntry(keyValue.Key);
                        SPFile spf = web.GetFile(keyValue.Value);
                        newEntry.DateTime = DateTime.Now;
                        zipStream.PutNextEntry(newEntry);
                        using (Stream fs = spf.OpenBinaryStream(SPOpenBinaryOptions.SkipVirusScan))
                        {
                            int sourceBytes;
                            do
                            {
                                sourceBytes = fs.Read(buffer, 0, buffer.Length);
                                zipStream.Write(buffer, 0, sourceBytes);
                            } while (sourceBytes > 0);
                        }
                        zipStream.CloseEntry();
                    }


                    zipStream.Finish();
                    // Close is important to wrap things up and unlock the file.
                    zipStream.Close();
                }
                SPFolder exportFolder = null;

                FolderExists(web.Url + "/" + ExportedDataLibName + "/" + EncodeName(person.LookupValue.Trim()), web, ref exportFolder);
                Stream oStream = File.OpenRead(fileName);
                exportFolder.Files.Add("Attachments.zip", oStream, true);
                oStream.Close();
                exportFolder.Update();

                System.IO.File.Delete(fileName);
            }

        }

        static void ZipIndDocs(SPWeb web, SPFieldLookupValue person)
        {
            SPFolder oIndFolder = null;
            bool bFolderExists = FolderExists(web.Url + "/IndDocs/" + EncodeName(person.LookupValue.Trim()), web, ref oIndFolder);
            if (bFolderExists == false)
            {
                CreateIndDocsFolder(web, EncodeName(person.LookupValue.Trim()));
                FolderExists(web.Url + "/IndDocs/" + EncodeName(person.LookupValue.Trim()), web, ref oIndFolder);
            }

            string fileName = "e:\\Temp\\IndDocs" + person.LookupId + "-" + DateTime.Now.ToString("yyyyMMddHHmm") + ".zip";

            using (ZipOutputStream zipStream = new ZipOutputStream(File.Create(fileName)))
            {
                zipStream.SetLevel(9);
                byte[] buffer = new byte[4096];
                ZipSubFolders(oIndFolder, buffer, zipStream, "");

                zipStream.Finish();
                // Close is important to wrap things up and unlock the file.
                zipStream.Close();
            }
            //bool bFileExists = FileExists(sFileAndPath, oIndFolder, ref oExport);
            //Thise line below can get an out of memory error because the entire file is read into memory.  Coding to use a FileStream, which will not!
            //oIndFolder.Files.Add(DefaultFileName, File.ReadAllBytes(fileName), true);
            SPFolder exportFolder = null;

            FolderExists(web.Url + "/" + ExportedDataLibName + "/" + EncodeName(person.LookupValue.Trim()), web, ref exportFolder);
            Stream oStream = File.OpenRead(fileName);
            exportFolder.Files.Add(DefaultZipFileName, oStream, true);
            oStream.Close();
            exportFolder.Update();

            System.IO.File.Delete(fileName);
        }

        static void ZipSubFolders(SPFolder fld, byte[] buffer, ZipOutputStream zipStream, string partFolderName)
        {
            string folderName = partFolderName + fld.Name;

            foreach (SPFile spf in fld.Files)
            {
                ZipEntry newEntry = new ZipEntry(folderName + "/" + spf.Name);
                newEntry.DateTime = DateTime.Now;
                zipStream.PutNextEntry(newEntry);
                using (Stream fs = spf.OpenBinaryStream(SPOpenBinaryOptions.SkipVirusScan))
                {

                    int sourceBytes;
                    do
                    {
                        sourceBytes = fs.Read(buffer, 0, buffer.Length);
                        zipStream.Write(buffer, 0, sourceBytes);
                    } while (sourceBytes > 0);
                }
                zipStream.CloseEntry();
            }

            foreach (SPFolder f in fld.SubFolders)
            {
                if (f.Name == "Forms")
                    continue;

                ZipSubFolders(f, buffer, zipStream, folderName + "/");
            }
        }
        
        static void CreateIndDocsFolder(SPWeb oWeb, string sInd)
        {
            try
            {
                SPList oIndDocs = oWeb.GetList(oWeb.Url + "/IndDocs/");
                SPListItem oListItem = oIndDocs.Folders.Add("", SPFileSystemObjectType.Folder, sInd);
                oListItem.Update();
            }
            catch (Exception ex)
            { }

        }

        static void CreateIndividualExportFolder(SPWeb oWeb, string sInd)
        {
            try
            {
                SPList oIndDocs = oWeb.GetList(oWeb.Url + "/" + ExportedDataLibName + "/");
                SPListItem oListItem = oIndDocs.Folders.Add("", SPFileSystemObjectType.Folder, sInd);
                oListItem.Update();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }



        static void FillAttendance(ExcelWorksheet ws, SPFieldLookupValue Person, SPWeb web)
        {
            SPWeb oWeb = null;
            SPSite oSite = null;
            int iRowNumber = 1;
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    oSite = new SPSite(web.Site.ID);
                    oWeb = oSite.OpenWeb();
                });
                
                string sql = " Attendance WHERE Attendance.IndividualId = '" + Person.LookupId + "'";
                System.Data.DataTable table = ReadFromDB(connectionString, sql);
                int iColumnNumber = 1;
                AddExcelCellTitle("Individual", ws, iRowNumber, iColumnNumber++);
                AddExcelCellTitle("Service", ws, iRowNumber, iColumnNumber++);
                AddExcelCellTitle("Date", ws, iRowNumber, iColumnNumber++);
                AddExcelCellTitle("Time In", ws, iRowNumber, iColumnNumber++);
                AddExcelCellTitle("Time Out", ws, iRowNumber, iColumnNumber++);
                AddExcelCellTitle("Activity", ws, iRowNumber, iColumnNumber++);
                AddExcelCellTitle("Sub-activity", ws, iRowNumber, iColumnNumber++);
                AddExcelCellTitle("Notes", ws, iRowNumber, iColumnNumber++);
                
                foreach (System.Data.DataRow dr in table.Rows)
                {
                    //Setup the Column and Row Numbers
                    iColumnNumber = 1;
                    iRowNumber++;
                    ws.Cells[iRowNumber, iColumnNumber].Value = Person.LookupValue;
                    iColumnNumber++;

                    int serviceId = Convert.ToInt32(dr["ProgramId"]);
                    DateTime date = Convert.ToDateTime(dr["Date"]);
                    string notes = dr["Notes"] + "";

                    int? activityId = null;
                    int? subActivityId = null;
                    DateTime? timeIn = null;
                    DateTime? timeOut = null;

                    string sTimeIn = "";
                    string sTimeOut = "";
                    string activity = "";
                    string subActivity = "";

                    if (dr.IsNull("ActivityId") == false)
                    {
                        activityId = Convert.ToInt32(dr["ActivityId"]);
                        activity = GetActivityName(activityId.Value, actList);
                    }
                    if (dr.IsNull("SubActivityId") == false)
                    {
                        subActivityId = Convert.ToInt32(dr["SubActivityId"]);
                        subActivity = GetActivityName(subActivityId.Value, subActList);
                    }
                    if (dr.IsNull("TimeIn") == false)
                    {
                        timeIn = Convert.ToDateTime(dr["TimeIn"]);
                        sTimeIn = timeIn.Value.ToString(timeFormat);
                    }
                    if (dr.IsNull("TimeOut") == false)
                    {
                        timeOut = Convert.ToDateTime(dr["TimeOut"]);
                        sTimeOut = timeOut.Value.ToString(timeFormat);
                    }

                    ws.Cells[iRowNumber, iColumnNumber].Value = GetServiceName(serviceId);
                    iColumnNumber++;
                    ws.Cells[iRowNumber, iColumnNumber].Value = date.ToString(dateFormat);
                    iColumnNumber++;
                    ws.Cells[iRowNumber, iColumnNumber].Value = sTimeIn;
                    iColumnNumber++;
                    ws.Cells[iRowNumber, iColumnNumber].Value = sTimeOut;
                    iColumnNumber++;
                    ws.Cells[iRowNumber, iColumnNumber].Value = activity;
                    iColumnNumber++;
                    ws.Cells[iRowNumber, iColumnNumber].Value = subActivity;
                    iColumnNumber++;
                    ws.Cells[iRowNumber, iColumnNumber].Value = notes;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);                
                throw ex;
            }
            finally
            {
                if (oWeb != null)
                    oWeb.Dispose();
                if (oSite != null)
                    oSite.Dispose();
            }
            
        }

        /* -- Do not have Comm Log feature installed.
        private int FillCommLog(ExcelWorksheet ws, SPFieldLookupValue Person, SPWeb web)

        {               
            string sql = @" 
                         CommunicationLog LEFT JOIN
                         CommunicationLogIndividual ON CommunicationLog.CommLogID = CommunicationLogIndividual.CommLogID LEFT JOIN
                         CommunicationLogService ON CommunicationLog.CommLogID = CommunicationLogService.CommLogID 
						 WHERE Deleted = 0 AND CommunicationLogIndividual.IndividualId = '" + Person.LookupId + "'";

            System.Data.DataTable table = ReadFromDB(connectionString, sql);

            int iColumnNumber = 1;
            int iRowNumber = 1;
            AddExcelCellTitle("Individual", ws, iRowNumber, iColumnNumber++);
            AddExcelCellTitle("Posted By", ws, iRowNumber, iColumnNumber++);
            AddExcelCellTitle("Comment Date", ws, iRowNumber, iColumnNumber++);
            AddExcelCellTitle("Comment", ws, iRowNumber, iColumnNumber++);
            AddExcelCellTitle("Service", ws, iRowNumber, iColumnNumber++);
            AddExcelCellTitle("Action Required", ws, iRowNumber, iColumnNumber++);
            AddExcelCellTitle("Completed", ws, iRowNumber, iColumnNumber++);
            AddExcelCellTitle("Urgent", ws, iRowNumber, iColumnNumber++);
            AddExcelCellTitle("Initialed By", ws, iRowNumber, iColumnNumber++);
            AddExcelCellTitle("Files", ws, iRowNumber, iColumnNumber++);

            foreach (System.Data.DataRow dr in table.Rows)
            {
                //Setup the Column and Row Numbers
                iColumnNumber = 1;
                iRowNumber++;
                ws.Cells[iRowNumber, iColumnNumber].Value = Person.LookupValue;
                iColumnNumber++;

                int postedById = Convert.ToInt32(dr["Staff"]);
                string comment = dr["Comment"] + "";
                DateTime commDate = Convert.ToDateTime(dr["DateCreated"]);
                bool action = Convert.ToBoolean(dr["ActionRequired"]);
                bool completed = Convert.ToBoolean(dr["IsComplete"]);
                bool urgent = Convert.ToBoolean(dr["IsUrgernt"]);
                int serviceId = Convert.ToInt32(dr["ServiceID"]);
                int commLogId = Convert.ToInt32(dr["CommLogID"]);
                string files = "";

                //get initialed by
                List<string> initByNames = new List<string>();
                string initSql = " CommunicationLogInitial WHERE CommLogID = " + commLogId;
                System.Data.DataTable initTable = ReadFromDB(connectionString, initSql);
                foreach (System.Data.DataRow dr2 in initTable.Rows)
                {
                    int initId = Convert.ToInt32(dr2["Staff"]);
                    initByNames.Add(GetStaffName(initId, web));
                }

                //get files?
                string attSql = " CommunicationLogAttachment WHERE CommLogID = " + commLogId;
                System.Data.DataTable attTable = ReadFromDB(connectionString, attSql);
                foreach (System.Data.DataRow dr2 in attTable.Rows)
                {
                    string att = dr2["AttachmentName"] + "";
                    files += att + "; ";
                }

                ws.Cells[iRowNumber, iColumnNumber].Value = GetStaffName(postedById, web);
                iColumnNumber++;
                ws.Cells[iRowNumber, iColumnNumber].Value = commDate.ToString(dateFormat);
                iColumnNumber++;
                ws.Cells[iRowNumber, iColumnNumber].Value = comment;
                iColumnNumber++;
                ws.Cells[iRowNumber, iColumnNumber].Value = GetServiceName(serviceId);
                iColumnNumber++;
                ws.Cells[iRowNumber, iColumnNumber].Value = action ? "Yes" : "No";
                iColumnNumber++;
                if (action)
                {
                    ws.Cells[iRowNumber, iColumnNumber].Value = completed ? "Yes" : "No";
                }
                else
                {
                    ws.Cells[iRowNumber, iColumnNumber].Value = "";
                }
                iColumnNumber++;
                ws.Cells[iRowNumber, iColumnNumber].Value = urgent ? "Yes" : "No";
                iColumnNumber++;
                ws.Cells[iRowNumber, iColumnNumber].Value = String.Join("; ", initByNames.ToArray());
                iColumnNumber++;
                ws.Cells[iRowNumber, iColumnNumber].Value = files;
            }

            return iRowNumber - 1;
            //rowCount = iRowNumber - 1;
        }
        */

        static void AddExcelCellTitle(string title, ExcelWorksheet ws, int iRowNumber, int iColumnNumber)
        {
            ws.Cells[iRowNumber, iColumnNumber].Value = title;
            ws.Cells[iRowNumber, iColumnNumber].Style.Font.Bold = true;
        }

        static SPListItemCollection GetIndividuals(SPWeb web)
        {
            SPListItemCollection InactiveIndividualsSelected = null;
            try
            {
                SPList SelectedIndividualsList = web.Lists[new Guid(ArchivingAndDeletionToolConfigurationListGuid)];
                SPQuery oQuery = new SPQuery();
                oQuery.Query = "<Where><And><IsNull><FieldRef Name='Status'/></IsNull><IsNotNull><FieldRef Name='InactiveIndividual'/></IsNotNull></And></Where>";
                oQuery.RowLimit = MaxIndividualsPerRun;

                InactiveIndividualsSelected = SelectedIndividualsList.GetItems(oQuery);
            }
            catch (Exception ex)
            {
                logger.Error("Error in GetIndividuals:" + ex.ToString());
                InactiveIndividualsSelected = null;
            }            

            return InactiveIndividualsSelected;
        }

        static string EncodeName(string sPerson)
        {
            //This will encode the name the same way that IndDocs.aspx does
            //Directly from SharePoint - Create Folder:
            //Invalid characters include the following: ~ " # % & * : < > ? / \ { | }. 
            //The name cannot begin or end with dot and cannot contains consecutive dots.
            string sOutput = sPerson;

            sOutput = sOutput.Replace("~", "_x7E_");
            sOutput = sOutput.Replace("\"", "_x22_");
            sOutput = sOutput.Replace("#", "_x23_");
            sOutput = sOutput.Replace("%", "_x25_");
            sOutput = sOutput.Replace("&", "_x26_");
            sOutput = sOutput.Replace("*", "_x2A_");
            sOutput = sOutput.Replace(":", "_x3A_");
            sOutput = sOutput.Replace("<", "_x3C_");
            sOutput = sOutput.Replace(">", "_x3E_");
            sOutput = sOutput.Replace("?", "_x3F_");
            sOutput = sOutput.Replace("/", "_x2F_");
            sOutput = sOutput.Replace("\\", "_x5C_");
            sOutput = sOutput.Replace("{", "_x7B_");
            sOutput = sOutput.Replace("|", "_x7C_");
            sOutput = sOutput.Replace("}", "_x7D_");
            if (sOutput.StartsWith(".") || sOutput.EndsWith("."))
                sOutput = sOutput.Replace(".", "_x2E_");

            return sOutput;
        }

        static string GetSafeListname(string sName)
        {
            //Similar to above, but don't put in the unescaped values, just remove the bad characters.
            string sOutput = sName;
            sOutput = sOutput.Replace("~", "");
            sOutput = sOutput.Replace("\"", "");
            sOutput = sOutput.Replace("#", "");
            sOutput = sOutput.Replace("%", "");
            sOutput = sOutput.Replace("&", "");
            sOutput = sOutput.Replace("*", "");
            sOutput = sOutput.Replace(":", "");
            sOutput = sOutput.Replace("<", "");
            sOutput = sOutput.Replace(">", "");
            sOutput = sOutput.Replace("?", "");
            sOutput = sOutput.Replace("/", "");
            sOutput = sOutput.Replace("\\", "");
            sOutput = sOutput.Replace("{", "");
            sOutput = sOutput.Replace("|", "");
            sOutput = sOutput.Replace("}", "");



            //To save Room in the Tab Name, it only is allowing 31 characters.
            sOutput = sOutput.Replace("-", "");
            sOutput = sOutput.Replace(" ", "");

            sOutput = sOutput.Replace("Archive", "ARC");
            sOutput = sOutput.Replace("Daily", "DLY");
            sOutput = sOutput.Replace("Individuals Information", "INDINFO");
            sOutput = sOutput.Replace("Information", "INFO");
            sOutput = sOutput.Replace("Individual", "IND");
            sOutput = sOutput.Replace("QuickStart", "QS");
            sOutput = sOutput.Replace("Communication", "COMM");
            sOutput = sOutput.Replace("Announcements", "ANC");
            sOutput = sOutput.Replace("Fact Sheet", "FS");
            sOutput = sOutput.Replace("Face Sheet", "FS");

            //March 22, 2016 Additions:
            sOutput = sOutput.Replace("Medication", "MED");
            sOutput = sOutput.Replace("Error", "ERR");
            sOutput = sOutput.Replace("Report", "RPT");
            sOutput = sOutput.Replace("Program", "PRG");
            sOutput = sOutput.Replace("Residence", "RES");
            sOutput = sOutput.Replace("Appointment", "APPT");
            sOutput = sOutput.Replace("Current", "CURR");
            sOutput = sOutput.Replace("Employee", "EMP");
            sOutput = sOutput.Replace("Assessment", "ASMT");
            sOutput = sOutput.Replace("Occurrence", "OCC");
            sOutput = sOutput.Replace("Training", "TRNG");
            sOutput = sOutput.Replace("Announcement", "ANC");
            sOutput = sOutput.Replace("Foster", "FSTR");
            sOutput = sOutput.Replace("Enrollment", "ENRL");
            sOutput = sOutput.Replace("Children", "CHDRN");
            //OnTrackBiologicalChildrenatEN

            if (sOutput.Length >= 30)
            {
                sOutput = sOutput.Substring(0, 29);
            }


            //if (sOutput == "OnTrackBiologicalCHDRNatENRL")
            //{
            //    logger.Info("Num of workbooks " + workbookNames.Count);

            //    foreach (string s in workbookNames)
            //    {
            //        logger.Info("NAME: " + s);
            //    }
            //}

            if (workbookNames.Contains(sOutput))
            {
                int counter = 2;
                while (workbookNames.Contains(sOutput + counter))
                {
                    counter++;
                }

                sOutput = sOutput + counter;
            }
            workbookNames.Add(sOutput);
            
            return sOutput;
        }

        static string GetExcelPathAndFilename(SPWeb oWeb, string sPerson, string DefaultFileName)
        {
            string path = oWeb.Url + "/" + ExportedDataLibName + "/" + EncodeName(sPerson) + "/" + DefaultFileName;
            return path;
        }

        static bool FolderExists(string url, SPWeb web, ref SPFolder oFolder)
        {
            try
            {
                //---Check folder exist or not
                if (web.GetFolder(url).Exists)
                {
                    oFolder = web.GetFolder(url);
                    return true;
                }
                return false;
            }
            catch (ArgumentException ex)
            {
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        static bool FileExists(string url, SPFolder oFolder, ref SPFile oFile)
        {
            try
            {
                //--Check file exist in the folder or not
                if (oFolder.Files[url].Exists)
                {
                    oFile = oFolder.Files[url];
                    return true;
                }
                return false;
            }
            catch (ArgumentException ex)
            {
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        static string GetDateFormat(SPWeb web)
        {
            SPRegionalSettings RegionSet = web.RegionalSettings;
            System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.GetCultureInfo(Convert.ToInt32(RegionSet.LocaleId));
            string dateFormat = ci.DateTimeFormat.ShortDatePattern;

            if (RegionSet.Time24)
            {
                dateFormat += " HH:mm";
            }
            else
            {
                dateFormat += " hh:mm tt";
            }
            return dateFormat;
        }

        static string GetTimeFormat(SPWeb web)
        {
            string timeFormat = "";
            SPRegionalSettings RegionSet = web.RegionalSettings;
            if (RegionSet.Time24)
            {
                timeFormat = "HH:mm";
            }
            else
            {
                timeFormat = "hh:mm tt";
            }
            return timeFormat;
        }

        static string GetActivityName(int activityId, SPList actList)
        {
            string name = "";
            try
            {
                SPListItem item = actList.GetItemById(activityId);
                name = item["Title"] + "";
            }
            catch (Exception ex)
            {

            }
            return name;
        }
        static string GetServiceName(int serviceId)
        {
            string name = "";
            try
            {
                SPListItem item = serviceList.GetItemById(serviceId);
                name = item["Title"] + "";
            }
            catch (Exception ex)
            {
            }
            return name;
        }

        static string GetStaffName(int id, SPWeb web)
        {
            string name = "";
            try
            {
                SPUser user = web.SiteUsers.GetByID(id);
                name = user.Name;
            }
            catch (Exception ex)
            {

            }
            return name;
        }

        static System.Data.DataTable ReadFromDB(string connectionString, string sql)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand selectCommand = new SqlCommand("SELECT * FROM " + sql, connection);
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = selectCommand;
                    System.Data.DataTable ds = new System.Data.DataTable();
                    adapter.Fill(ds);
                    selectCommand.Dispose();
                    adapter.Dispose();

                    return ds;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
        }

        static System.Data.DataTable DeleteAttendanceFromDB(string indId)
        {            
            if (String.IsNullOrEmpty(indId))
                return null;

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand deleteCommand = new SqlCommand("DELETE FROM [dbo].[Attendance] WHERE [IndividualId] = " + indId, connection);
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.DeleteCommand = deleteCommand;                    
                    int numberOfRecordsDeleted = deleteCommand.ExecuteNonQuery(); 

                    System.Data.DataTable ds = new System.Data.DataTable();
                    
                    deleteCommand.Dispose();
                    adapter.Dispose();
                    return ds;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
        }

        static string SafelyGetTextFromColumn(SPField oField, SPListItem oItem)
        {
            try
            {
                string myValue = oField.GetFieldValueAsText(oItem[oField.Id]);
                if (String.IsNullOrEmpty(myValue))
                    return " ";

                return myValue;
            }
            catch (Exception ex)
            { }
            return " ";
        }

        //Beginning Helper Functions for Delete Person:
        
        static void DeletePerson(SPWeb web, SPFieldLookupValue person, List<SPList> IndividualLists)
        {
            DeleteAttendanceFromDB(person.LookupId + "");

            for (int x = 0; x < IndividualLists.Count; x++)
            {
                SPList list = IndividualLists[x];
                
                string listName = "";
                try
                {
                    listName = list.Fields[0].Title; //This will ensure we have a working list.
                    listName = list.Title;
                }
                catch (Exception ex)
                {
                    continue;
                }

                bool bListHasIndividualField = false;
                string sQuery = "";
                foreach (SPField oField in list.Fields)
                {
                    if (oField.Reorderable)
                    {
                        if (oField.Type == SPFieldType.Lookup || oField.TypeDisplayName == "Filtered Lookup" || oField.TypeDisplayName == "Filtered Lookup field")
                        {
                            SPFieldLookup oLookupField = (SPFieldLookup)oField;

                            if (oLookupField.LookupList == "{703fd232-253a-4d00-bac8-cccf2367c36d}" && oLookupField.LookupField == "FullName" && oLookupField.AllowMultipleValues == false)
                            {
                                bListHasIndividualField = true;

                                string tempQ = "<Eq><FieldRef Name='" + oLookupField.InternalName + "' LookupId='true' /><Value Type='Lookup'>" + person.LookupId + "</Value></Eq>";

                                if (oField.Indexed == true)
                                {
                                    tempQ = "<Eq><FieldRef Name='" + oLookupField.InternalName + "' /><Value Type='Lookup'>" + person.LookupValue + "</Value></Eq>";
                                }

                                if (sQuery == "")
                                    sQuery = tempQ;
                                else
                                    sQuery = "<Or>" + tempQ + sQuery + "</Or>";
                            }
                        }
                    }
                }

                if (bListHasIndividualField)
                {
                    bool bHasContentTypesOn = list.ContentTypesEnabled;
                    SPQuery Query2 = new SPQuery();
                    Query2.Query = "<Where>" + sQuery + "</Where>";
                    SPListItemCollection oListItems = list.GetItems(Query2);
                    int rowCount = oListItems.Count;

                    if (oListItems.Count > 0)
                    {
                        //logger.Info("list " + list.Title + " - " + rowCount);

                        for (int i = oListItems.Count - 1; i > -1; i--)
                        {
                            oListItems[i].Delete();
                        }
                    }
                }
            }


            //delete docs
            SPFolder oIndFolder = null;
            bool bFolderExists = FolderExists(web.Url + "/IndDocs/" + EncodeName(person.LookupValue.Trim()), web, ref oIndFolder);
            if (bFolderExists)
            {
                oIndFolder.Delete();
                //logger.Info("Deleted Ind Docs");
            }

            //delete people item
            SPListItem peopleItem = web.Lists[new Guid("{703fd232-253a-4d00-bac8-cccf2367c36d}")].GetItemById(person.LookupId);
            peopleItem.Delete();
            //logger.Info("Deleted People Item: ");
        }
        
    }
}
